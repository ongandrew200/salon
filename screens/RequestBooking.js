import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ImageBackground, ScrollView, TextInput } from 'react-native';
import { Calendar } from 'react-native-big-calendar'
import * as Animatable from 'react-native-animatable';
import Feather from 'react-native-vector-icons/Feather';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Dialog from "react-native-dialog";

const RequestBookingScreen = ({ navigation }) => {
  const [visible, setVisible] = useState(false); //register cancel dialog box
  const [backVisible, setBackVisible] = useState(false); //register cancel dialog box
  const [viewServicesVisible, setViewServicesVisible] = useState(false); //register cancel dialog box
  const [hrFmt, setHrFmt] = useState('');
  const [date, setDate] = useState(1);
  const [month, setMonth] = useState(1);
  const [year, setYear] = useState(1);
  const [hours, setHours] = useState(0);
  const [min, setMinutes] = useState(0);
  const [desc, setDesc] = useState('');

  const showDialog = () => {
    setVisible(true);
  };
  const showBackDialog = () => {
    setBackVisible(true);
  };
  const handleCloseBack = () => {
    setBackVisible(false);
  };
  const showLoginSuccessSnackbar = () => {
    setLoginSuccessVisible(!visible);
  };
  const onDismissSnackBar = () => {
    setLoginSuccessVisible(false)
  };
  const handleCancel = () => {
    setVisible(false);
  };
  const showViewServicesVisible = () => {
    setViewServicesVisible(true);
  };
  const dismissViewServicesVisible = () => {
    setViewServicesVisible(false);
  }
  useEffect(() => {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var hours = new Date().getHours(); //Current Hours
    var min = new Date().getMinutes(); //Current Minutes
    setHours(hours);
    setMinutes(min);
    setDate(date);
    setYear(year);
    setMonth(month);
    if (min < 10) {
      min = '0' + min;
    }
  }, []);
  return (
    <ImageBackground source={require('../img/HomeScreenBackground.jpg')} resizeMode="cover" style={styles.image}>

      <View style={[styles.mainFlex, { flex: 0, marginTop: header_margin, marginBottom: header_margin }]}>
        <View>
          <Animatable.Image
            animation="bounceIn"
            source={require('../img/HairDo.png')}
            style={[styles.logo]}
          />
          <Text style={styles.helpbtn_txt, { textTransform: 'uppercase' }}>Hair DO Salon</Text>
        </View>
        <Animatable.View style={{ flex: 1 }} animation="fadeInDown" duration={1000} delay={1000}>
          <View style={{ alignSelf: 'flex-end' }}>
            <TouchableOpacity onPress={() => navigation.navigate('FAQ')}>
              <Animatable.View
                animation="bounceIn"
              >
                <Feather
                  name="help-circle"
                  color="#D7BF5E"
                  size={44}
                  style={[styles.user_logo, { left: 0 }]}
                />
                <Text style={styles.helpbtn_txt}>HELP</Text>
              </Animatable.View>
            </TouchableOpacity>
          </View>
        </Animatable.View>

      </View>
      <Animatable.View style={styles.timeline} animation="fadeInDown" duration={3000} delay={1000}>
        <Text style={styles.timelineText}>Request Bookings</Text>
      </Animatable.View>
      <ScrollView>
        <View style={[styles.aboutUsWrapper
          , { marginTop: 0 }
        ]}
        >
          <Text style={styles.aboutUsHeader}>Select Date</Text>
        </View>


        <View style={styles.mainFlex}>
          <TextInput
            placeholder="Input Day"
            style={styles.timeInput}
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={2}
            onChangeText={(val, month) => {
              if (isNaN(val)) {
                console.log(val);
                setDate(1)
                // console.log(age);
                // console.log(target.valueAsNumber);
              }
              else if (val == ' ') {
                setDate(NaN)
              }
              else if (val >= 31) {
                setDate(31)
              }
              else {
                let numberValue = parseInt(val)
                setDate(Math.max(1, Math.floor(numberValue)))
              }

            }}
            InputProps={{ inputProps: { min: 1 } }}
            value={isNaN(date) ? '' : date.toString()}
          />
          <TextInput
            placeholder="Input Month"
            style={styles.timeInput}
            keyboardType="numeric"
            maxLength={2}
            onChangeText={(val) => {
              if (isNaN(val)) {
                console.log(val);
                setMonth(0)
                // console.log(age);
                // console.log(target.valueAsNumber);
              }
              else if (val == ' ') {
                setMonth(NaN)
              }
              else if (val >= 31) {
                setMonth(31)
              }
              else {
                let numberValue = parseInt(val)
                setMonth(Math.max(0, Math.floor(numberValue)))
              }
            }}
            InputProps={{ inputProps: { min: 0 } }}
            value={isNaN(month) ? '' : month.toString()}
          />
          <TextInput
            placeholder="Input Year"
            style={styles.timeInput}
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={4}
            onChangeText={(val) => {
              if (isNaN(val)) {
                console.log(val);
                setYear(year)
              }
              else if (val == ' ') {
                setYear(NaN)
              }
              else {
                let numberValue = parseInt(val)
                setYear(Math.max(1, Math.floor(numberValue)))
              }


            }}
            InputProps={{ inputProps: { min: 1 } }}
            value={isNaN(year) ? '' : year.toString()}
          />


        </View>
        <View style={[styles.aboutUsWrapper, { width: width_logo }]}>
          <Text style={styles.aboutUsHeader}>Select Start Time (24 hours format)</Text>
        </View>
        <View style={styles.mainFlex}>
          <TextInput
            placeholder="Input Hour"
            placeholderTextColor="grey"
            style={styles.timeInput}
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={2}
            onChangeText={(val) => {
              if (isNaN(val)) {
                console.log(val);
                setHours(0)
                // console.log(age);
                // console.log(target.valueAsNumber);
              }
              else if (val == ' ') {
                setHours(NaN)
              }
              else if (val >= 24) {
                setHours(23)
              }
              else {
                let numberValue = parseInt(val)
                setHours(Math.max(1, Math.floor(numberValue)))

              }

            }}
            InputProps={{ inputProps: { min: 1 } }}
            value={isNaN(hours) ? '' : hours.toString()}
          // onChangeText={(val) => textInputChange(val)}
          />
          <TextInput
            placeholder="Input Minute"
            style={styles.timeInput}
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={2}
            onChangeText={(val) => {
              if (isNaN(val)) {
                console.log(val);
                setMinutes(0)
                // console.log(age);
                // console.log(target.valueAsNumber);
              }
              else if (val == ' ') {
                setMinutes(NaN)
              }
              else if (val >= 60) {
                setMinutes(59)
              }
              else {
                let numberValue = parseInt(val)
                setMinutes(Math.max(0, Math.floor(numberValue)))
              }


            }}
            InputProps={{ inputProps: { min: 0 } }}
            value={isNaN(min) ? '' : min.toString()}
          />



        </View>
        <View style={[styles.aboutUsWrapper, { width: width_logo }]}>
          <Text style={styles.aboutUsHeader}>Select End Time (24 hours format)</Text>
        </View>
        <View style={styles.mainFlex}>
          <TextInput
            placeholder="Input Hour"
            placeholderTextColor="grey"
            style={styles.timeInput}
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={2}
            onChangeText={(val) => {
              if (isNaN(val)) {
                console.log(val);
                setHours(0)
                // console.log(age);
                // console.log(target.valueAsNumber);
              }
              else if (val == ' ') {
                setHours(NaN)
              }
              else if (val >= 24) {
                setHours(23)
              }
              else {
                let numberValue = parseInt(val)
                setHours(Math.max(1, Math.floor(numberValue)))

              }

            }}
            InputProps={{ inputProps: { min: 1 } }}
            value={isNaN(hours) ? '' : hours.toString()}
          // onChangeText={(val) => textInputChange(val)}
          />
          <TextInput
            placeholder="Input Minute"
            style={styles.timeInput}
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={2}
            onChangeText={(val) => {
              if (isNaN(val)) {
                console.log(val);
                setMinutes(0)
                // console.log(age);
                // console.log(target.valueAsNumber);
              }
              else if (val == ' ') {
                setMinutes(NaN)
              }
              else if (val >= 60) {
                setMinutes(59)
              }
              else {
                let numberValue = parseInt(val)
                setMinutes(Math.max(0, Math.floor(numberValue)))
              }


            }}
            InputProps={{ inputProps: { min: 0 } }}
            value={isNaN(min) ? '' : min.toString()}
          />



        </View>

        <View style={styles.mainFlex}>
          <View style={[styles.aboutUsWrapper]}>
            <Text style={styles.aboutUsHeader}>Select Service</Text>
          </View>

          <View style={{ marginRight: 10 }}></View>

          <View style={[styles.aboutUsWrapper]}>
            {/* <TouchableOpacity onPress={showViewServicesVisible}> */}
            <TouchableOpacity onPress={() => navigation.navigate('ServicesScreen')}>
              <Animatable.View
                animation="bounceIn"
              >
                <Text style={styles.aboutUsHeader}>View Services</Text>
              </Animatable.View>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.mainFlex}>
          <TextInput
            placeholder="Services Description"
            style={[styles.timeInput, { width: logOut_button_width }]}
            autoCapitalize="none"
            onChangeText={(val) => setDesc(val)}
          />
        </View>


        <View style={styles.mainFlex}>
          <Animatable.View style={styles.flex} animation="zoomInDown" duration={2000} delay={1000}>

            <TouchableOpacity
              onPress={showBackDialog}
              style={[styles.button, styles.Buttonflex]}
            >
              {/* <View style={[styles.helpBox,styles.backFont, { marginTop: 60 }]}> */}
              <View style={[styles.helpBox, styles.helpBox4]}>
                <Text style={styles.helpBoxText}>Back</Text>
              </View>
            </TouchableOpacity>
          </Animatable.View>

          <Animatable.View style={styles.flex} animation="zoomInDown" duration={2000} delay={1000}>

            <TouchableOpacity
              onPress={showDialog}
              style={[styles.button, styles.Buttonflex]}
            >
              {/* <View style={[styles.helpBox,styles.backFont, { marginTop: 60 }]}> */}
              <View style={[styles.helpBox, styles.helpBox4]}>
                <Text style={styles.helpBoxText}>Confirm</Text>
              </View>
            </TouchableOpacity>
          </Animatable.View>
          <Dialog.Container visible={visible}>

            <Dialog.Title style={styles.contactUsNow}>
              Your Booking has been requested successfully !
            </Dialog.Title>

            <Dialog.Button label="View Booking" onPress={() => {
              navigation.navigate('MyBookingScreen');
              // setLogOutSuccess(true);
            }}
              style={styles.DialogButton} />
            <Dialog.Button label="Request Another Booking"
              onPress={handleCancel}
              style={styles.DialogButton}
            />
          </Dialog.Container>
          <Dialog.Container visible={backVisible}>
            <Dialog.Title>
              Are you sure you want to cancel?
            </Dialog.Title>
            <Dialog.Title>
              The data key will be clear?
            </Dialog.Title>
            <Dialog.Button
              label="Yes"
              onPress={() => {
                navigation.goBack();
                // setLogOutSuccess(true);
              }}
              style={[styles.DialogButton, styles.DialogYesNo]}
            />
            <Dialog.Button
              label="No"
              onPress={handleCloseBack}
              style={[styles.DialogButton, styles.DialogYesNo]}
            />
          </Dialog.Container>
          <Dialog.Container visible={viewServicesVisible} contentStyle={{ backgroundColor: '#facd91' }}>

            <Dialog.Title style={styles.contactUsNow}>
              Type of services provided
            </Dialog.Title>
            <Dialog.Description style={styles.contactUsDesc}>
              Cut
            </Dialog.Description>
            <Dialog.Description style={styles.contactUsDesc}>
              Cut + Wash
            </Dialog.Description>
            <Dialog.Description style={styles.contactUsDesc}>
              Dyeing
            </Dialog.Description>
            <Dialog.Description style={styles.contactUsDesc}>
              Treatment
            </Dialog.Description>
            <Dialog.Description style={styles.contactUsDesc}>
              Perming
            </Dialog.Description>
            <Dialog.Button label="OK" onPress={dismissViewServicesVisible} />
          </Dialog.Container>
        </View>
      </ScrollView>
    </ImageBackground >

  );
};


export default RequestBookingScreen;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");

const full_width = width * 1;
const width_logo = width * 0.5;
const button_width = width * 0.40;
const logOut_button_width = width * 0.75;
const width_user_text = width * 0.3;
const timeWidth = width * 0.2;
const aboutUsHeader = width * 0.035;

const height_Dialog = height * 0.75;
const ourBranchesHeight = height * 0.48;
const height_logo = height * 0.15;
const header_margin = height * 0.05;
const back_selection = height * 0.03;
const textInput = height * 0.02;
const footerButton = height * 0.01;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    alignItems: 'center'
  },
  logo: {
    width: 100,
    height: 75,
    // alignItems: 'flex-start',
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold'
  },
  timeline: {
    backgroundColor: '#d9d9d9',
    height: 46,
  },

  timelineText: {
    textAlign: 'center',
    fontSize: 30,
    color: '#7B4D12FE',
    fontWeight: "600",
    textDecorationLine: 'underline',
    fontFamily: 'Lora-Italic-VariableFont_wght',
    textTransform: 'uppercase',
  },
  text: {
    color: 'grey',
    marginTop: 5
  },
  flex: {
    flex: 1,
    // left: width_user,
    // top: selection,
    textAlign: 'center',
    alignItems: 'center',
  },
  Headerflex: {
    flex: 1,
    // left: width_user,
    top: footerButton,
    textAlign: 'center',
    alignItems: 'center',
  },

  Buttonflex: {
    flex: 1,
    textAlign: 'center',
    alignItems: 'center',
  },
  mainFlex: {
    flex: 2,
    flexDirection: 'row',
  },
  myProfileMainFlex: {
    flex: 0.5,
    flexDirection: 'row',
  },
  button: {
    alignItems: 'center',
    marginTop: 30,
    // bottom: selection,
  },
  signIn: {
    width: logOut_button_width,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#7b4d12',
    borderWidth: 1,
    borderStyle: 'solid',
  },
  textSign: {
    color: '#7B4D12FE',
    fontSize: 28,
    textAlign: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontSize: 40,
    fontFamily: 'Cochin',
    color: '#7B4D12FE',
    fontWeight: 'bold',
    textDecorationLine: 'underline'
  },
  image: {
    flex: 1,
    // justifyContent: "center"
  },
  helpbtn_txt: {
    color: 'white',
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: 15,
    fontWeight: "800",
  },
  ourBranchesHeight: {
    height: ourBranchesHeight
  },
  ourBranchesImage: {
    width: 128,
    height: 128,
    borderRadius: 128 / 2,
    marginTop: 10,
    flex: 1,
    marginRight: 20,
  },
  ourBranchText: {
    color: 'black',
    // marginTop: 20,
    fontSize: 15,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    // width: width_user_text,
    alignSelf: 'center',
  },
  helpBox: {
    height: 45,
    width: button_width,
    borderColor: 'black',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    bottom: back_selection,
    marginTop: 50,
  },
  helpBox4: {
    backgroundColor: '#fd6c9e',
  },
  helpBoxText: {
    textAlign: 'center',
    // marginTop: 10,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: 28,
    color: 'black',
  },
  RadioButtonText: {
    color: 'white',
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: 15,
    top: 7,
  },
  aboutUsWrapper: {
    backgroundColor: 'rgba(250, 205, 145, 1)',
    // position: 'absolute',
    left: 20,
    marginTop: height_logo,
    width: button_width,
    height: back_selection,
    borderRadius: textInput,
  },
  aboutUsHeader: {
    color: '#653507',
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 5,
    fontSize: aboutUsHeader,
    fontFamily: 'BeautifulEveryTime-Dg4m',

  },
  HairDo: {
    fontWeight: 'bold',
  },
  aboutUsText: {
    color: 'black',
    marginTop: 5,
    // textTransform: 'uppercase',
    fontSize: 15,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    backgroundColor: 'white',
    // height: 100,
    width: width_user_text,
    alignSelf: 'center',
    paddingLeft: 10,
    paddingTop: 2,
  },
  DialogButton: {
    width: button_width,
    justifyContent: 'space-between',
  },
  DialogYesNo: {
    borderWidth: 2,
    borderColor: 'brown',
    backgroundColor: '#CEFA05',
    color: 'black',
  },
  inputBorder: {
    width: '30%',
    borderRadius: 8,
    borderColor: '#cacaca',
    borderWidth: 1,
    marginBottom: 20,
    textDecorationColor: 'white',
    color: 'white',
  },
  DialogHeight: {
    height: height_Dialog,
  },
  contactUsNow: {
    color: '#653507',
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 2,
    // textTransform: 'uppercase',
    fontSize: 25,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  contactUsDesc: {
    color: '#653507',
    textAlign: 'center',
    marginTop: 2,
    fontSize: 20,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  timeInput: {
    width: timeWidth,
    height: header_margin,
    backgroundColor: 'white',
    borderColor: 'brown',
    borderRadius: textInput,
    borderWidth: 2,
    paddingLeft: 10,
    marginLeft: 20,
    marginTop: 10,
    fontSize: textInput,
    color: 'black',
  },
});




