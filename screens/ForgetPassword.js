import React, { useState, useEffect, Children } from 'react';
import { View, StyleSheet, Dimensions, Text, Image, TouchableOpacity, ImageBackground, ScrollView, TextInput } from 'react-native';
import * as Animatable from 'react-native-animatable';
import Feather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import Dialog from "react-native-dialog";

const ForgotPassword = ({ navigation }) => {

    const [backVisible, setBackVisible] = useState(false); //register cancel dialog box

    const showBackDialog = () => {
        setBackVisible(true);
    };
    const handleCloseBack = () => {
        setBackVisible(false);
    };
    return (
        <ImageBackground source={require('../img/Background.png')} resizeMode="cover" style={styles.image}>
            <ScrollView>
                <View style={styles.helpbtn}>
                    <TouchableOpacity onPress={() => navigation.navigate('FAQ')}>
                        <Animatable.View
                            animation="bounceIn"
                        >
                            <Feather
                                name="help-circle"
                                color="white"
                                size={44}
                            />
                            <Text style={styles.helpbtn_txt}>HELP</Text>
                        </Animatable.View>
                    </TouchableOpacity>
                </View>

                <View style={{ height: header_height }}>
                    <Animatable.Image
                        animation="bounceIn"
                        source={require('../img/HairDo.png')}
                        style={[styles.logo]}
                        resizeMode="stretch"
                    />
                    <Text style={styles.headerText}>Forgot Password</Text>
                </View>
                <View style={styles.myProfileContent}>
                    <Text style={styles.ForgotPasswordText}>Instruction ...</Text>
                    <Text style={styles.ForgotPasswordText}>1. Key In Phone Number</Text>
                    <Text style={styles.ForgotPasswordText}>2. Send code</Text>
                    <Text style={styles.ForgotPasswordText}>3. Verify the code</Text>
                </View>
                <View style={styles.action}>
                    {/* <FontAwesome
                        name="user-o"
                        size={30}
                    /> */}
                    <View style={[styles.aboutUsWrapper
                        , { marginTop: 0 }
                    ]}
                    >
                        <Text style={styles.aboutUsHeader}>Phone Number: </Text>
                    </View>



                    <TextInput
                        placeholder="Input Phone Number"
                        style={[styles.timeInput, { width: ForgotPasswordWidth, alignSelf: 'center' }]}
                        autoCapitalize="none"
                        keyboardType="numeric"
                        maxLength={10}
                    />
                    {/* {data.check_textInputChange ?
                        <Animatable.View
                            animation="bounceIn"
                        >
                            <Feather
                                name="check-circle"
                                color="green"
                                size={20}
                            />
                        </Animatable.View>
                        : null} */}
                </View>
                <View style={styles.action}>
                    {/* <FontAwesome
                        name="user-o"
                        size={30}
                    /> */}
                    <View style={[styles.aboutUsWrapper
                        , { marginTop: 0 }
                    ]}
                    >
                        <Text style={styles.aboutUsHeader}>Code : </Text>
                    </View>


                    <View style={styles.mainFlex}>
                        <TextInput
                            placeholder="OTP code"
                            style={[styles.timeInput, { flex: 1, marginLeft: shorterTime }]}
                            autoCapitalize="none"
                            keyboardType="numeric"
                            maxLength={10}
                        />
                        <TouchableOpacity style={{ justifyContent: 'center' }}>
                            <LinearGradient
                                colors={['#facd91', '#facd91']}
                                style={[styles.signIn, { width: timeWidth, height: 45, justifyContent: 'center', marginRight: timeWidth }]}
                            >
                                <Text style={styles.textSign, { textAlign: 'center' }}>Send
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>


                </View>
                <View style={styles.mainFlex}>
                    <Animatable.View style={styles.flex} animation="zoomInDown" duration={2000} delay={1000}>

                        <TouchableOpacity
                            onPress={showBackDialog}
                            style={[styles.button, styles.Buttonflex]}
                        // onPress={() => navigation.goBack()}
                        >
                            {/* <View style={[styles.helpBox,styles.backFont, { marginTop: 60 }]}> */}
                            <View style={[styles.helpBox, { backgroundColor: '#fd6c9e' }]}>
                                <Text style={styles.helpBoxText}>Back</Text>
                            </View>
                        </TouchableOpacity>
                    </Animatable.View>

                    <Animatable.View style={styles.flex} animation="zoomInDown" duration={2000} delay={1000}>

                        <TouchableOpacity
                            // onPress={showDialog}
                            style={[styles.button, styles.Buttonflex]}
                            onPress={() => navigation.goBack()}
                        >
                            {/* <View style={[styles.helpBox,styles.backFont, { marginTop: 60 }]}> */}
                            <View style={[styles.helpBox, { backgroundColor: '#fd6c9e' }]} >
                                <Text style={styles.helpBoxText}>Verify</Text>
                            </View>
                        </TouchableOpacity>
                    </Animatable.View>
                </View>
                <Dialog.Container visible={backVisible}>
                    <Dialog.Title>
                        Are you sure you want to cancel?
                    </Dialog.Title>
                    <Dialog.Title>
                        The data key will be clear?
                    </Dialog.Title>
                    <Dialog.Button label="Yes" onPress={() => {
                        navigation.goBack();
                    }} />
                    <Dialog.Button label="No" onPress={handleCloseBack} />
                </Dialog.Container>
            </ScrollView>

        </ImageBackground >
    )

}
export default ForgotPassword;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");
const ForgotPasswordWidth = width * 0.6;
const image_Width = width * 0.4;
const timeWidth = width * 0.2;
const shorterTime = width * 0.18;
const font_Size = width * 0.07;
const middle_font_Size = width * 0.045;
const aboutUsHeader = width * 0.035;

const header_height = height * 0.3;
const image_height = height * 0.2;
const header_margin = height * 0.05;
const textInput = height * 0.02;


const styles = StyleSheet.create({
    image: {
        flex: 1,
        justifyContent: "center",
    },
    helpbtn: {
        alignItems: 'flex-end',
        marginRight: 10,
        marginTop: 20,
    },
    helpbtn_txt: {
        color: "white",
        fontFamily: 'Lora-Italic-VariableFont_wght',
        fontSize: 15,
        fontWeight: "800",
    },
    logo: {
        width: image_Width,
        height: image_height,
        alignSelf: 'center',
    },
    headerText: {
        fontSize: font_Size,
        color: '#ffd700',
        fontFamily: 'Lora-Italic-VariableFont_wght',
        textDecorationLine: 'underline',
        textAlign: 'center',
        textTransform: 'uppercase',
    },
    myProfileContent: {
        borderWidth: 2,
        borderColor: 'white',
        borderRadius: 10,
        backgroundColor: 'rgba(248, 192, 108, 0.8)',
        width: ForgotPasswordWidth,
        alignSelf: 'center',
        padding: 20,

    },
    ForgotPasswordText: {
        fontSize: middle_font_Size,
        color: 'brown',
        fontFamily: 'Lora-Italic-VariableFont_wght',

    },
    timeInput: {
        width: timeWidth,
        height: header_margin,
        backgroundColor: 'white',
        borderColor: 'brown',
        borderRadius: textInput,
        borderWidth: 2,
        paddingLeft: 10,
        marginRight: middle_font_Size,
        fontSize: textInput,
        color: 'black',
    },
    aboutUsHeader: {
        color: 'white',
        // textAlign: 'center',
        marginLeft: timeWidth,
        marginTop: 5,
        fontSize: aboutUsHeader,
        fontFamily: 'BeautifulEveryTime-Dg4m',
        marginTop: textInput,

    },
    mainFlex: {
        flex: 2,
        flexDirection: 'row',
    },
    flex: {
        flex: 1,
        // left: width_user,
        // top: selection,
        textAlign: 'center',
        alignItems: 'center',
    },
    button: {
        alignItems: 'center',
        marginTop: 30,
        // bottom: selection,
    },
    Buttonflex: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
    },
    helpBox: {
        height: 45,
        width: image_Width,
        borderColor: 'black',
        borderWidth: 1,
        borderStyle: 'solid',
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center',
        bottom: textInput,
        marginTop: 50,
    },
    Buttonflex: {
        flex: 1,
        textAlign: 'center',
        alignItems: 'center',
    },
    helpBoxText: {
        textAlign: 'center',
        // marginTop: 10,
        fontFamily: 'Lora-Italic-VariableFont_wght',
        fontSize: 28,
        color: 'black',
    },
    signIn: {
        width: timeWidth,
        height: 45,
        justifyContent: 'center',
        marginRight: timeWidth,
        borderRadius: 15,
    }
})