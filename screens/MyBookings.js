import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';


const MyBookingScreen = ({ navigation }) => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000)
  }, []);//the array here meaning when something changes only useeffect will take effect

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <OrientationLoadingOverlay visible={true}>
          <View>
            {/* <Image source={require('./img/Salon.gif')} style={{ width: 150, height: 150 }} /> */}
            <Image source={require('../img/Salon.gif')} style={{ width: 200, height: 200 }} />

          </View>
        </OrientationLoadingOverlay>
      </View>
    )
  }
  return (
    <ImageBackground source={require('../img/sunSet.jpg')} resizeMode="cover" style={styles.image}>

      <View>
        <Text style={styles.headerLogoTxt}>Hair Do Salon</Text>
      </View>
      <View style={styles.aboutUsWrapper}>
        <Text style={styles.aboutUsHeader}>Stylist</Text>
      </View>
      <View style={[styles.aboutUsWrapper, { marginTop: 0 }]}>
        <Text style={styles.aboutUsHeader}>Appointment Details</Text>
      </View>
      <View style={styles.ourBranchesHeight}>
        <ScrollView>
          <View style={styles.mainFlex}>
            <Image source={require('../img/ZhaoLiYing.jpg')} style={styles.ourBranchesImage} />
            <View style={styles.verticalLine}></View>
            <View style={styles.ourBranchViewWrapper}>

              <Text style={styles.ourBranchText}>Date: June 30 2021</Text>
              <Text style={styles.ourBranchText}>Time: 10:00 - 12:00</Text>
              <Text style={styles.ourBranchText}>Name: Gok Kut</Text>
              <Text style={styles.ourBranchText}>Description : Hair Perming</Text>
              <Text style={styles.ourBranchText}>No. People: 5</Text>
              <Text style={styles.ourBranchText}>Stylist : Zhao Li Ying</Text>
            </View>
          </View>
          <View style={styles.mainFlex}>
            <Image source={require('../img/KarenGillan.jpg')} style={styles.ourBranchesImage} />
            <View style={styles.verticalLine}></View>
            <View style={styles.ourBranchViewWrapper}>
              <Text style={styles.ourBranchText}>Date: July 1 2021</Text>
              <Text style={styles.ourBranchText}>Time: 12:00 - 14:00</Text>
              <Text style={styles.ourBranchText}>Name: Gok Kut</Text>
              <Text style={styles.ourBranchText}>Description : Hair Colouring</Text>
              <Text style={styles.ourBranchText}>No. People: 5</Text>
              <Text style={styles.ourBranchText}>Stylist :Karen Gillan</Text>
            </View>
          </View>
          <View style={styles.mainFlex}>
            <Image source={require('../img/GalGadot.jpg')} style={styles.ourBranchesImage} />
            <View style={styles.verticalLine}></View>
            <View style={styles.ourBranchViewWrapper}>
              <Text style={styles.ourBranchText}>Date: June 30 2021</Text>
              <Text style={styles.ourBranchText}>Time: 10:00 - 12:00</Text>
              <Text style={styles.ourBranchText}>Name: Gok Kut</Text>
              <Text style={styles.ourBranchText}>Description : Hair Perming</Text>
              <Text style={styles.ourBranchText}>No. People: 5</Text>
              <Text style={styles.ourBranchText}>Stylist : Gal Gadot</Text>
            </View>
          </View>
          <View style={styles.mainFlex}>
            <Image source={require('../img/ElizabethOlsen.jpg')} style={styles.ourBranchesImage} />
            <View style={styles.verticalLine}></View>
            <View style={styles.ourBranchViewWrapper}>
              <Text style={styles.ourBranchText}>Date: June 30 2021</Text>
              <Text style={styles.ourBranchText}>Time: 10:00 - 12:00</Text>
              <Text style={styles.ourBranchText}>Name: Gok Kut</Text>
              <Text style={styles.ourBranchText}>Description : Hair Charming</Text>
              <Text style={styles.ourBranchText}>No. People: 5</Text>
              <Text style={styles.ourBranchText}>Stylist : Elizabeth Olsen</Text>
            </View>
          </View>
          <View style={styles.mainFlex}>
            <Image source={require('../img/JenniferLawrence.jpg')} style={styles.ourBranchesImage} />
            <View style={styles.verticalLine}></View>
            <View style={styles.ourBranchViewWrapper}>
              <Text style={styles.ourBranchText}>Date: June 30 2021</Text>
              <Text style={styles.ourBranchText}>Time: 10:00 - 12:00</Text>
              <Text style={styles.ourBranchText}>Name: Gok Kut</Text>
              <Text style={styles.ourBranchText}>Description : Hair Perming</Text>
              <Text style={styles.ourBranchText}>No. People: 5</Text>
              <Text style={styles.ourBranchText}>Stylist : Jennifer Lawrence</Text>
            </View>
          </View>


        </ScrollView>
      </View>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        {/* <View style={[styles.helpBox,styles.backFont, { marginTop: 60 }]}> */}
        <View style={[styles.helpBox, styles.helpBox4]}>
          <Text style={styles.helpBoxText}>Back</Text>
        </View>
      </TouchableOpacity>

    </ImageBackground>

  );
};


export default MyBookingScreen;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");
// const height_logo = height * 0.28;
const button_width = width * 0.75;
const logOut_button_width = width * 0.75;
const height_logo = height * 0.1;
const selection = height * 0.03;
const back_selection = height * 0.03;
const aboutUsHeight = height * 0.13;
const ourBranchesHeight = height * 0.55;
const width_logo = width * 0.4;
const width_user = width * 0.1;
const width_user_text = width * 0.9;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    width: 110,
    height: 110,
    alignSelf: 'center',
    position: 'absolute',
    justifyContent: 'center', //horizontal align
    alignSelf: 'center',
    marginTop: 11,

  },
  user_logo: {
    // position: 'absolute',
    // top: 0,
    left: 20,
    color: 'black',
    marginTop: 10
  },
  user_logo_text: {
    position: 'absolute',
    top: 30,
    marginLeft: width_user_text,
    color: 'black',
    fontSize: 18,
    textDecorationLine: 'underline'
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold'
  },
  timeline: {
    backgroundColor: '#facd91',
    height: 20,
    textAlign: 'center',
    alignContent: 'center',
    marginTop: 20,
  },

  timelineText: {
    marginLeft: width_user_text,
  },
  text: {
    color: 'grey',
    marginTop: 5
  },
  selectionText: {
    // left: 10,
    color: 'black',
    marginTop: 10,
    top: 10,
    fontSize: 18,
    left: 5,
  },
  iconFlex: {
    // position: 'absolute',
    // top: 0,
    color: 'black',
    marginTop: 10
  },
  flex: {
    flex: 1,
    // left: width_user,
    top: selection,
    // textAlign: 'center',
    // alignItems: 'center',
  },
  mainFlex: {
    flex: 4,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'brown',
    borderWidth: 2,
  },

  button: {
    alignItems: 'center',
    marginTop: 30,
    bottom: selection,
  },
  signIn: {
    width: logOut_button_width,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#7b4d12',
    borderWidth: 1,
    borderStyle: 'solid',
  },
  textSign: {
    color: '#7B4D12FE',
    fontSize: 28,
    textAlign: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontSize: 40,
    fontFamily: 'Cochin',
    color: '#7B4D12FE',
    fontWeight: 'bold',
    textDecorationLine: 'underline'
  },
  image: {
    flex: 1,
    // justifyContent: "center"
  },
  headerLogoTxt: {
    color: "yellow",
    fontSize: 25,
    alignSelf: 'center',
    position: 'absolute',
    top: selection,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 20
  },
  timeline: {
    backgroundColor: '#D7BF5E',
    height: 20,
    textAlign: 'center',
    alignContent: 'center',
    marginTop: 20,
  },
  aboutUsWrapper: {
    backgroundColor: 'rgba(250, 205, 145, 1)',
    // position: 'absolute',
    left: 20,
    marginTop: height_logo,
    width: 250,
    height: 40,
  },
  aboutUsHeader: {
    color: '#653507',
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 5,
    // textTransform: 'uppercase',
    fontSize: 20,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  HairDo: {
    fontWeight: 'bold',
  },
  aboutUsText: {
    color: 'black',
    marginTop: 5,
    // textTransform: 'uppercase',
    fontSize: 15,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    backgroundColor: 'white',
    // height: 100,
    width: width_user_text,
    alignSelf: 'center',
    paddingLeft: 10,
    paddingTop: 2,
  },

  ourBranchWrapper: {
    backgroundColor: 'rgba(250, 205, 145, 1)',
    left: 20,
    width: 172,
    height: 40,
  },
  ourBranchHeader: {
    color: '#653507',
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 5,
    fontSize: 25,
    fontFamily: 'BeautifulEveryTime-Dg4m',

  },
  ourBranchViewWrapper: {
    flex: 2,
    paddingLeft: 20,
    paddingRight: 10,
    // width: width_user_text,
  },
  ourBranchText: {
    color: 'black',
    fontSize: 15,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    backgroundColor: 'white',
    paddingTop: 2,
    left: 20,
    right: 20,
  },
  ourBranchesImage: {
    width: 132,
    height: 132,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
    left: 20,
    flex: 1,

  },
  ourBranchesFlex: {
    display: 'flex',
  },
  ourBranchesHeight: {
    height: ourBranchesHeight,
    marginLeft: 20,
    marginRight: 20,
    marginTop:5,
  },
  aboutUsHeight: {
    height: aboutUsHeight,
    marginBottom: 10,
  },
  helpBox: {
    height: 45,
    width: button_width,
    borderColor: 'black',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
    // bottom: back_selection,
  },
  helpBox4: {
    backgroundColor: '#fd6c9e',
  },
  helpBoxText: {
    textAlign: 'center',
    // marginTop: 10,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: 28,
    color: 'black',
  },
  contactUsNow: {
    color: '#653507',
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 2,
    // textTransform: 'uppercase',
    fontSize: 25,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  contactUsDesc: {
    color: '#653507',
    textAlign: 'center',
    marginTop: 2,
    fontSize: 19,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  verticalLine: {
    height: '100%',
    width: 5,
    backgroundColor: 'brown',
    left: 40,
    marginRight: 10,
  }
});



