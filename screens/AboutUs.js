import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import Dialog from "react-native-dialog";

const AboutUs = ({ navigation }) => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [visible, setVisible] = useState(false);

  const showDialog = () => {
    setVisible(true);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000)
  }, []);//the array here meaning when something changes only useeffect will take effect

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <OrientationLoadingOverlay visible={true}>
          <View>
            {/* <Image source={require('./img/Salon.gif')} style={{ width: 150, height: 150 }} /> */}
            <Image source={require('../img/Salon.gif')} style={{ width: 200, height: 300 }} />

          </View>
        </OrientationLoadingOverlay>
      </View>
    )
  }

  return (
    <ImageBackground source={require('../img/Background.png')} resizeMode="cover" style={{ flex: 1 }}>
      <View>
        <Animatable.Image
          animation="bounceIn"
          source={require('../img/HairDo.png')}
          style={styles.logo}
          resizeMode="stretch"
        />
        <Text style={styles.headerLogoTxt}>Hair Do Salon</Text>
      </View>
      <View style={styles.aboutUsWrapper}>
        <Text style={styles.aboutUsHeader}>About Us</Text>
      </View>

      <View style={styles.aboutUsHeight}>
        <ScrollView>
          <Text style={styles.aboutUsText}>HairDo saloon is a hair saloon established in B.C. 900 across every
            timeline and universe.We have gotten mutiple of awards including Grammy, Oscar, NewYork
            Fashionista and The Infinity War.
            We have collaborate with all the Marvel heroes including Thanos and Hulk.
          </Text>
        </ScrollView>
      </View>
      <View style={styles.ourBranchWrapper}>
        <Text style={styles.ourBranchHeader}>Our Branches</Text>
      </View>
      <View style={{ height: ourBranchesHeight }}>
        <ScrollView>
          <View style={styles.mainFlex}>
            <Image source={require('../img/asgard.png')} style={styles.ourBranchesImage} />

            <Text style={styles.ourBranchText}>Asgard of Midgard of Son of Thor , Odinson , Loki , Asgard is not the place , its the people , 43040, Asgard</Text>
          </View>
          <View style={styles.mainFlex}>
            <Image source={require('../img/quantumRealm.jpg')} style={styles.ourBranchesImage} />

            <Text style={styles.ourBranchText}>60, Jln AntmanWasp , Bandar Time works differently here, 444000, Quantum</Text>
          </View>
          <View style={styles.mainFlex}>
            <Image source={require('../img/saaa.png')} style={styles.ourBranchesImage} />

            <Text style={styles.ourBranchText}>Space waste planet , Jalan Thor and Hulk, City of GrandMaster & Valkrie , 122200 , Sakkaaarrr </Text>
          </View>
          <View style={styles.mainFlex}>
            <Image source={require('../img/titan.png')} style={styles.ourBranchesImage} />

            <Text style={styles.ourBranchText}>The desolated home planet , Titans , Galaxy 1113 </Text>
          </View>


        </ScrollView>
      </View>
      <TouchableOpacity
        // onPress={() => navigation.navigate('SplashScreen')}
        onPress={showDialog}
        style={styles.button}>
        <LinearGradient
          colors={['#facd91', '#facd91']}
          style={[styles.signIn, { marginTop: 20 }]}
        >
          <Text style={[styles.textSign, {
          }]}>Contact Us</Text>
        </LinearGradient>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        {/* <View style={[styles.helpBox,styles.backFont, { marginTop: 60 }]}> */}
        <View style={[styles.helpBox, { backgroundColor: '#fd6c9e', }]}>
          <Text style={styles.helpBoxText}>Back</Text>
        </View>
      </TouchableOpacity>
      {/* <Dialog.Container visible={visible} contentStyle={{backgroundColor:'#facd91'}}> */}
      <Dialog.Container visible={visible}>
        <Dialog.Title style={styles.contactUsNow}>
          Contact us now!
        </Dialog.Title>
        <Dialog.Description style={styles.contactUsDesc}>
          Email:HairDo@gmail.com
        </Dialog.Description>
        <Dialog.Description style={styles.contactUsDesc}>
          Tel  :03-22221111
        </Dialog.Description>
        <Dialog.Description style={styles.contactUsDesc}>
          Whatsapp:hairdodododo
        </Dialog.Description>
        <Dialog.Button
          label="OK"
          onPress={handleCancel}
          style={styles.OKButton}
        />
      </Dialog.Container>
    </ImageBackground>

  );
};


export default AboutUs;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");

const width_user_text = width * 0.9;
const button_width = width * 0.75;
const width_logo = width * 0.4;
const width_user = width * 0.1;


const height_logo = height * 0.20;
const selection = height * 0.05;
const back_selection = height * 0.05;
const aboutUsHeight = height * 0.13;
const ourBranchesHeight = height * 0.3;


const styles = StyleSheet.create({
  logo: {
    width: 110,
    height: 110,
    alignSelf: 'center',
    position: 'absolute',
    justifyContent: 'center', //horizontal align
    alignSelf: 'center',
    marginTop: 11,

  },
  flex: {
    flex: 1,
    top: selection,
  },
  mainFlex: {
    flex: 4,
    flexDirection: 'row',
  },
  button: {
    alignItems: 'center',
    marginTop: 30,
    bottom: selection,
  },
  signIn: {
    width: button_width,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#7b4d12',
    borderWidth: 1,
    borderStyle: 'solid',
  },
  textSign: {
    color: '#7B4D12FE',
    fontSize: 28,
    textAlign: 'center',
    justifyContent: 'center',
  },
  headerLogoTxt: {
    color: "#D7BF5E",
    fontSize: 15,
    alignSelf: 'center',
    position: 'absolute',
    top: 105,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  aboutUsWrapper: {
    backgroundColor: 'rgba(250, 205, 145, 1)',
    // position: 'absolute',
    left: 20,
    marginTop: height_logo,
    width: 172,
    height: 40,
  },
  aboutUsHeader: {
    color: '#653507',
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 5,
    // textTransform: 'uppercase',
    fontSize: 25,
    fontFamily: 'BeautifulEveryTime-Dg4m',

  },
  HairDo: {
    fontWeight: 'bold',
  },
  aboutUsText: {
    color: 'black',
    marginTop: 5,
    // textTransform: 'uppercase',
    fontSize: 15,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    backgroundColor: 'white',
    // height: 100,
    width: width_user_text,
    alignSelf: 'center',
    paddingLeft: 10,
    paddingTop: 2,
  },

  ourBranchWrapper: {
    backgroundColor: 'rgba(250, 205, 145, 1)',
    left: 20,
    width: 172,
    height: 40,
  },
  ourBranchHeader: {
    color: '#653507',
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 5,
    fontSize: 25,
    fontFamily: 'BeautifulEveryTime-Dg4m',

  },

  ourBranchText: {
    color: 'black',
    marginTop: 20,
    fontSize: 15,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    backgroundColor: 'white',
    width: width_user_text,
    alignSelf: 'center',
    paddingLeft: 10,
    paddingTop: 2,
    flex: 2,
    marginLeft: 40,
    right: 10,
  },
  ourBranchesImage: {
    width: 128,
    height: 128,
    borderRadius: 128 / 2,
    marginTop: 20,
    left: 20,
    flex: 1,
  },
  helpBox: {
    height: 45,
    width: button_width,
    borderColor: 'black',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    // marginTop: back_selection,
  },
  helpBoxText: {
    textAlign: 'center',
    // marginTop: 10,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: 28,
    color: 'black',
  },
  contactUsNow: {
    color: '#653507',
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 2,
    // textTransform: 'uppercase',
    fontSize: 25,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  contactUsDesc: {
    color: '#653507',
    textAlign: 'center',
    marginTop: 2,
    fontSize: 19,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  OKButton: {
    backgroundColor: 'orange',
    borderColor: 'brown',
    borderWidth: 1,
    borderRadius: 10,
    width: width_logo,
    textAlign: 'center',
    fontFamily: 'Lora-Italic-VariableFont_wght',
    color: '#653507',
  },
});



