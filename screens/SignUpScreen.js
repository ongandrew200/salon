import React, { useState, useEffect } from 'react';
import { ImageBackground, Image, View, Text, Button, TouchableOpacity, Dimensions, TextInput, Platform, StyleSheet, ScrollView, StatusBar } from 'react-native';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import Dialog from "react-native-dialog";
import auth from '@react-native-firebase/auth';
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from 'react-native-google-signin';

const SignInScreen = ({ navigation }) => {

    const [visible, setVisible] = useState(false); //register cancel dialog box
    const [loggedIn, setloggedIn] = useState(false);
    const [userInfo, setuserInfo] = useState([]);

    const [data, setData] = React.useState({
        username: '',
        password: '',
        confirm_password: '',
        phoneNum: 0,
        check_textInputChange: false,
        secureTextEntry: true,
        confirmSecureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
        isValidConfirmPassword: true,
        isValidPhoneNum: true,
    });

    const googleSignIn = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const { accessToken, idToken } = await GoogleSignin.signIn();
            setloggedIn(true);
            const credential = auth.GoogleAuthProvider.credential(
                idToken,
                accessToken,
            );
            await auth().signInWithCredential(credential);

        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
                alert('Cancel');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                alert('Signin in progress');
                // operation (f.e. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                alert('PLAY_SERVICES_NOT_AVAILABLE');
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    };

    useEffect(() => {
        GoogleSignin.configure({
            scopes: ['email'], // what API you want to access on behalf of the user, default is email and profile
            webClientId:
                '303780915676-lrgjad8csk2i519fc382fhdmd9janf59.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        });
    }, []);

    const textInputChange = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                username: val,
                check_textInputChange: true,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                username: val,
                check_textInputChange: false,
                isValidUser: false
            });
        }
    }

    const handlePasswordChange = (val) => {
        if (val.trim().length >= 8) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            });
        } else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });
        }
    }

    const handleConfirmPasswordChange = (val) => {
        if (val.trim().length >= 8) {
            setData({
                ...data,
                password: val,
                isValidConfirmPassword: true
            });
        } else {
            setData({
                ...data,
                password: val,
                isValidConfirmPassword: false
            });
        }
    }
    // const handleSamePassword = (val) => {
    //     if (val.trim().length >= 8) {


    //             isValidConfirmPassword: true

    //     } else {

    //             isValidConfirmPassword: false

    //     }
    // }

    const handlePhoneNum = (val) => {
        if (val.trim().length >= 10) {
            setData({
                ...data,
                phoneNum: val,
                isValidPhoneNum: true
            });
        } else {
            setData({
                ...data,
                phoneNum: val,
                isValidPhoneNum: false
            });
        }
    }


    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    const updateConfirmSecureTextEntry = () => {
        setData({
            ...data,
            confirmSecureTextEntry: !data.confirmSecureTextEntry
        });
    }

    const showDialog = () => {
        setVisible(true);
    };
    const handleDelete = () => {
        // The user has pressed the "Delete" button, so here you can do your own logic.
        // ...Your logic
        setVisible(false);
    };
    return (
        <ImageBackground source={require('../img/Background.png')} resizeMode="cover" style={styles.image}>
            <ScrollView>
                <View style={styles.helpbtn}>
                    <TouchableOpacity onPress={() => navigation.navigate('FAQ')}>
                        <Animatable.View
                            animation="bounceIn"

                        >
                            <Feather
                                name="help-circle"
                                color="white"
                                size={44}
                            />
                            <Text style={styles.helpbtn_txt}>HELP</Text>
                        </Animatable.View>
                    </TouchableOpacity>
                </View>
                <View>
                    <Animatable.Image
                        animation="bounceIn"
                        source={require('../img/HairDo.png')}
                        style={[styles.logo]}
                        resizeMode="stretch"
                    />
                    <Text style={styles.headerText}>REGISTER</Text>
                </View>
                <Animatable.View
                    animation="fadeInUpBig"
                    style={styles.footer}
                >
                    <ScrollView>
                        <Text style={styles.text_footer}>User Email</Text>
                        <View style={styles.action}>
                            <FontAwesome
                                name="user-o"
                                color="#05375a"
                                size={20}
                            />
                            <TextInput
                                placeholder="Your User Email"
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                            />
                            {data.check_textInputChange ?
                                <Animatable.View
                                    animation="bounceIn"
                                >
                                    <Feather
                                        name="check-circle"
                                        color="green"
                                        size={20}
                                    />
                                </Animatable.View>
                                : null}
                        </View>
                        {data.isValidUser ? null :
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>*Username must be 4 characters long.</Text>
                            </Animatable.View>
                        }


                        <Text style={[styles.text_footer, {
                            marginTop: 35
                        }]}>Password</Text>
                        <View style={styles.action}>
                            <Feather
                                name="lock"
                                color="#05375a"
                                size={20}
                            />
                            <TextInput
                                placeholder="Your Password"
                                secureTextEntry={data.secureTextEntry ? true : false}
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => handlePasswordChange(val)}
                            />
                            <TouchableOpacity
                                onPress={updateSecureTextEntry}
                            >
                                {data.secureTextEntry ?
                                    <Feather
                                        name="eye-off"
                                        color="grey"
                                        size={20}
                                    />
                                    :
                                    <Feather
                                        name="eye"
                                        color="grey"
                                        size={20}
                                    />
                                }
                            </TouchableOpacity>
                        </View>
                        {data.isValidPassword ? null :
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>*Password must be 8 characters long.</Text>
                            </Animatable.View>
                        }

                        <Text style={[styles.text_footer, {
                            marginTop: 35
                        }]}>Confirm Password</Text>
                        <View style={styles.action}>
                            <Feather
                                name="lock"
                                color="#05375a"
                                size={20}
                            />
                            <TextInput
                                placeholder="Confirm Your Password"
                                secureTextEntry={data.confirmSecureTextEntry ? true : false}
                                style={styles.textInput}
                                autoCapitalize="none"
                                onChangeText={(val) => handleConfirmPasswordChange(val)}
                            />
                            <TouchableOpacity
                                onPress={updateConfirmSecureTextEntry}
                            >
                                {data.confirmSecureTextEntry ?
                                    <Feather
                                        name="eye-off"
                                        color="grey"
                                        size={20}
                                    />
                                    :
                                    <Feather
                                        name="eye"
                                        color="grey"
                                        size={20}
                                    />
                                }
                            </TouchableOpacity>
                        </View>
                        {data.isValidConfirmPassword ? null :
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>*Password must be 8 characters long.</Text>
                            </Animatable.View>
                        }
                        {/* <Text style={[styles.text_footer, {
                            marginTop: 35
                        }]}>Phone Number</Text> */}
                        {/* <View style={styles.action}>
                            <Feather
                                name="smartphone"
                                color="#05375a"
                                size={20}
                            />
                            <TextInput
                                placeholder="Your Phone Number"
                                style={styles.textInput}
                                autoCapitalize="none"
                                keyboardType="numeric"
                                type="number"
                                onChangeText={(val) => handlePhoneNum(val)}
                                maxLength={10}
                            />

                        </View> */}
                        {data.isValidPhoneNum ? null :
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>*Please key in proper phone number</Text>
                            </Animatable.View>
                        }
                        <View style={styles.textPrivate}>
                            <Text style={styles.color_textPrivate}>
                                By signing up you agree to our
                            </Text>
                            <Text style={[styles.color_textPrivate, { fontWeight: 'bold' }]}>{" "}Terms of service</Text>
                            <Text style={styles.color_textPrivate}>{" "}and</Text>
                            <Text style={[styles.color_textPrivate, { fontWeight: 'bold' }]}>{" "}Privacy policy</Text>
                        </View>
                        <Animatable.View style={styles.button} animation="fadeInUpBig">
                            <TouchableOpacity
                                style={styles.signIn}
                                // onPress={() => { }}
                                onPress={() => {
                                    auth()
                                        .createUserWithEmailAndPassword(data.username, data.password)
                                        .then(() => {
                                            navigation.navigate('SuccessScreen');
                                        })
                                        .catch(error => {
                                            if (error.code === 'auth/email-already-in-use') {
                                                console.log('That email address is already in use!');
                                            }

                                            if (error.code === 'auth/invalid-email') {
                                                console.log('That email address is invalid!');
                                            }

                                            console.error(error);
                                        });
                                }}
                            >
                                <LinearGradient
                                    colors={['#ffff80', '#ffff80']}
                                    style={styles.signIn}
                                >
                                    <Text style={[styles.textSign, {

                                    }]}>Register</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            <GoogleSigninButton
                                style={{ width: button_width, height: logo_height_post, marginTop: 15 }}
                                size={GoogleSigninButton.Size.Wide}
                                color={GoogleSigninButton.Color.Dark}
                                onPress={googleSignIn}
                            />
                            {loggedIn && (
                                <Button
                                    onPress={this.signOut}
                                    title="LogOut"
                                    color="red"></Button>
                            )}
                            <TouchableOpacity onPress={showDialog}>
                                <LinearGradient
                                    colors={['#facd91', '#facd91']}
                                    style={[styles.signIn, { marginTop: 15 }]}
                                >
                                    <Text style={[styles.textSign, {
                                    }]}>Cancel</Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('SignInScreen')}>

                                <Text style={[styles.NewUser, { color: '#7B4D12FE' }]}>Already Have An Account?
                                    <Text style={styles.NewUser}> Try Login In! </Text>
                                </Text>

                            </TouchableOpacity>
                        </Animatable.View>
                    </ScrollView>
                </Animatable.View>
                <Dialog.Container visible={visible} contentStyle={{ backgroundColor: '#facd91' }}>
                    <Dialog.Title>
                        Are you sure you want to cancel?
                    </Dialog.Title>
                    <Dialog.Title>
                        The data key will be clear?
                    </Dialog.Title>
                    <Dialog.Button label="Yes" onPress={() => {
                        navigation.navigate('SplashScreen');
                        // setLogOutSuccess(true);
                    }}
                        style={[styles.DialogButton, styles.DialogYesNo, { backgroundColor: '#ffff80', marginLeft: 0 }]} />
                    <Dialog.Button label="No"
                        onPress={handleDelete}
                        style={[styles.DialogButton, styles.DialogYesNo, { backgroundColor: '#f59a22' }]}
                    />
                </Dialog.Container>
            </ScrollView >
        </ImageBackground>
    );
};

export default SignInScreen;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");

const button_width = width * 0.50;
const image_Width = width * 0.4;
const font_Size = width * 0.07;
const tertiary_font_Size = width * 0.04;
const forth_font_Size = width * 0.03;
const fifth_font_Size = width * 0.02;

const header_height = height * 0.4;
const footer_height = height * 0.52;
const image_height = height * 0.2;
const boxHeight = height * 0.06;
const logo_height_post = height * 0.05;

const styles = StyleSheet.create({
    header: {
        height: header_height,
    },
    footer: {
        height: footer_height,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 20,
    },

    headerText: {
        fontSize: font_Size,
        color: '#ffd700',
        fontFamily: 'Lora-Italic-VariableFont_wght',
        textDecorationLine: 'underline',
        textAlign: 'center',
        textTransform: 'uppercase',
        marginBottom: fifth_font_Size,
    },
    text_footer: {
        color: '#05375a',
        fontSize: tertiary_font_Size,
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
        fontSize: forth_font_Size,
        height: boxHeight,
    },
    errorMsg: {
        marginTop: 10,
        letterSpacing: 1.5,
        color: '#FF0000',
        fontSize: forth_font_Size,
        fontFamily: 'Lora-Italic-VariableFont_wght',
    },
    button: {
        alignItems: 'center',
        marginTop: 30,
    },
    signIn: {
        width: button_width,
        height: logo_height_post,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        flexDirection: 'row',
        borderColor: '#7b4d12',
        borderWidth: 1,
        borderStyle: 'solid',
    },
    textSign: {
        color: '#7B4D12FE',
        fontSize: 28,
        textAlign: 'center',
        justifyContent: 'center',
        fontFamily: 'Lora-Italic-VariableFont_wght',
    },
    NewUser: {
        marginTop: 10,
        letterSpacing: 1.5,
        color: '#FF0000',
        fontSize: forth_font_Size,
        fontFamily: 'Lora-Italic-VariableFont_wght',
    },
    logo: {
        width: image_Width,
        height: image_height,
        alignSelf: 'center',
    },
    image: {
        flex: 1,
        justifyContent: "center"
    },
    helpbtn: {
        alignItems: 'flex-end',
        marginRight: 10,
        marginTop: 20,
    },
    helpbtn_txt: {
        color: "white",
        fontFamily: 'Lora-Italic-VariableFont_wght',
        fontSize: 15,
        fontWeight: "800",
    },
    DialogButton: {
        width: image_Width,
        justifyContent: 'space-between',
    },
    DialogYesNo: {
        borderWidth: 2,
        borderColor: 'brown',
        color: 'black',
        marginLeft: fifth_font_Size,
    },
    textPrivate: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 20
    },
    color_textPrivate: {
        color: 'grey',
        fontSize: forth_font_Size,
    }

});