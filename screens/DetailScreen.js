import React from 'react';
import { View, Text, StyleSheet, Button, Image } from 'react-native';

const DetailScreen = ({ navigation }) => {
    return (
      <View style={styles.sectionContainer}>
        <Text>Detail Screen</Text>
        <Button title="Go to Navigation Screen again ... "
          //{navigation} is a set 
          onPress={() => navigation.navigate("Details")}></Button>
        <Button title="Go to Home "
          //{navigation} is a set 
          onPress={() => navigation.navigate("Home")}></Button>
        <Button title="Go Back"
          //{navigation} is a set 
          onPress={() => navigation.goBack()}></Button>
        <Button title="Go to First Screen"
          //{navigation} is a set 
          onPress={() => navigation.popToTop()}></Button>
      </View>
    );
  };

  export default DetailScreen;
  
const styles = StyleSheet.create({
    sectionContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    backgroundImage: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      opacity: 0.3
    }
  
  });
