import React, { useState, useEffect } from 'react';
import { Button, View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Dialog from "react-native-dialog";
import { Snackbar } from 'react-native-paper';
import Feather from 'react-native-vector-icons/Feather';
import { yellow100 } from 'react-native-paper/lib/typescript/styles/colors';
import TimeDisplay from './TimeDisplay';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import auth from '@react-native-firebase/auth';

const SuccessScreen = ({ navigation }) => {
  // const [currentDate, setCurrentDate] = useState('');

  // const [interval, setInterval] = useState(1000);
  // const [open, setOpen] = useState(false);//for add
  const [isLoading, setIsLoading] = React.useState(true);
  const [visible, setVisible] = useState(false);
  const [logOutSuccess, setLogOutSuccess] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 2000)
  }, []);//the array here meaning when something changes only useeffect will take effect

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <OrientationLoadingOverlay visible={true}>
          <View>
            {/* <Image source={require('./img/Salon.gif')} style={{ width: 150, height: 150 }} /> */}
            <Image source={require('../img/Salon.gif')} style={{ width: 200, height: 300 }} />

          </View>
        </OrientationLoadingOverlay>
      </View>
    )
  }

  // const handleOpen = () => {
  //   setOpen(true);
  // };
  // const handleClose = () => {
  //   setOpen(false);
  // };
  const showDialog = () => {
    setVisible(true);
  };

  const handleCancel = () => {
    setVisible(false);
  };
  const logOutFunc = () => {
    // navigation.navigate('SplashScreen');
    setLogOutSuccess(true);
  }

  const handleDelete = () => {
    // The user has pressed the "Delete" button, so here you can do your own logic.
    // ...Your logic
    setVisible(false);
  };
  return (
    <View style={styles.container}>
      <ImageBackground source={require('../img/HomeScreenBackground.jpg')} resizeMode="cover" style={styles.image}>

        <View style={[styles.mainFlex, { flex: 0, marginTop: header_margin, marginBottom: header_margin }]}>
          {/* <View style={styles.headerLogo}> */}

          {/* </View> */}
          <Animatable.View style={styles.myProfileHelpFlex} animation="fadeInDown" duration={1000} delay={1000}>
            <View style={{ alignSelf: 'flex-start' }}>
              <TouchableOpacity onPress={() => navigation.navigate('MyProfileScreen')}>
                <Animatable.View
                  animation="bounceIn"
                >
                  <EvilIcons
                    name="user"
                    color="#D7BF5E"
                    size={65}
                    style={[styles.user_logo]}
                  />
                  <Text style={styles.profilebtn_txt}>My Profile</Text>
                </Animatable.View>
              </TouchableOpacity>
            </View>
          </Animatable.View>

          {/* <Animatable.Text style={styles.headerLogoTxt} animation="rubberBand" duration={3000} delay={1000}>Hair Do Salon</Animatable.Text> */}

          <Animatable.Image
            animation="bounceIn"
            source={require('../img/HairDo.png')}
            style={styles.logo}
          />
          <Animatable.View style={styles.myProfileHelpFlex} animation="fadeInDown" duration={1000} delay={1000}>
            <View style={{ alignSelf: 'flex-end' }}>
              <TouchableOpacity onPress={() => navigation.navigate('FAQ')}>
                <Animatable.View
                  animation="bounceIn"
                >
                  <Feather
                    name="help-circle"
                    color="#D7BF5E"
                    size={44}
                    style={[styles.user_logo, { left: 0 }]}
                  />
                  <Text style={styles.helpbtn_txt}>HELP</Text>
                </Animatable.View>
              </TouchableOpacity>
            </View>
          </Animatable.View>

        </View>
        <Text style={styles.headerText}>Welcome To HairDo Salon !</Text>
        <TimeDisplay />

        <View style={styles.mainFlex}>
          <Animatable.View style={styles.flex} animation="zoomInDown" duration={3000} delay={1000}>
            <TouchableOpacity onPress={() => navigation.navigate('RequestBookingScreen')}>

              <AntDesign
                name="calendar"
                size={80}
                style={[styles.iconFlex]}
              />
              <Text style={styles.selectionText}>Request Booking</Text>
            </TouchableOpacity>
          </Animatable.View>
          <Animatable.View style={styles.flex} animation="zoomInRight" duration={3000} delay={1500}>
            <TouchableOpacity onPress={() => navigation.navigate('ServicesScreen')}>

              <FontAwesome
                name="scissors"
                size={80}
                style={[styles.iconFlex]}
              />
              <Text style={styles.selectionText}>Services</Text>
            </TouchableOpacity>
          </Animatable.View>
        </View>
        <View style={styles.mainFlex}>
          <Animatable.View style={styles.flex} animation="zoomInLeft" duration={3000} delay={2000}>
            <TouchableOpacity onPress={() => navigation.navigate('MyBookingScreen')}>

              <MaterialCommunityIcons
                name="clipboard-list-outline"
                size={80}
                style={[styles.iconFlex]}
              />
              <Text style={styles.selectionText}>My Bookings</Text>
            </TouchableOpacity>
          </Animatable.View>
          <Animatable.View style={styles.flex} animation="zoomInUp" duration={3000} delay={2500}>
            <TouchableOpacity onPress={() => { navigation.navigate('AboutUsScreen') }}>

              <MaterialCommunityIcons
                name="cellphone-information"
                size={80}
                style={[styles.iconFlex]}
              />
              <Text style={styles.selectionText}>About Us</Text>
            </TouchableOpacity>
          </Animatable.View>
        </View>
        <TouchableOpacity
          // onPress={() => navigation.navigate('SplashScreen')}
          onPress={showDialog}
          style={styles.button}>
          <LinearGradient
            colors={['#facd91', '#facd91']}
            style={[styles.signIn, { marginTop: 15 }]}
          >
            <Text style={[styles.textSign, {
            }]}>Log Out</Text>
          </LinearGradient>
        </TouchableOpacity>
        {/* <Button title="Log Out" style={styles.button} onPress={showDialog} /> */}

        <Dialog.Container visible={visible} contentStyle={{ backgroundColor: '#facd91' }}>

          <Dialog.Title style={styles.contactUsNow}>
            Are you sure you wanna log out?
          </Dialog.Title>

          <Dialog.Button label="Yes" onPress={() => {
            auth()
              .signOut()
              .then(() => {
                navigation.navigate('SplashScreen');
              })
            setLogOutSuccess(true);
          }}
            style={[styles.DialogButton, styles.DialogYesNo, { backgroundColor: '#ffff80', marginLeft: 0 }]} />
          <Dialog.Button label="No"
            onPress={handleDelete}
            style={[styles.DialogButton, styles.DialogYesNo, { backgroundColor: '#f59a22' }]}
          />

        </Dialog.Container>

        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          bodyStyle={{
            width: 300,
            height: 200,
            flexGrow: 0,
          }}
          open={logOutSuccess}
          onClose={() => {
            setLogOutSuccess(false)
          }

          }
          autoHideDuration={3000}
        >
          Log Out Successfully!

        </Snackbar>
      </ImageBackground>
    </View>

  );
};


export default SuccessScreen;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");
// const height_logo = height * 0.28;

const logOut_button_width = width * 0.75;
const height_logo = height * 0.16;
const selection = height * 0.1;
const header_margin = height * 0.05;
const back_selection = height * 0.04;

const button_width = width * 0.3;
const help_width = width * 0.2;
const secondary_font_Size = width * 0.05;
const middle_font_Size = width * 0.045;
const image_Width = width * 0.4;
const tertiary_font_Size = width * 0.035;
const fifth_font_Size = width * 0.02;


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    width: help_width,
    height: help_width,
  },
  user_logo: {
    left: 10,
    color: '#D7BF5E',
    textAlign: 'center',
    alignItems: 'center',
  },
  user_logo_text: {
    // position: 'absolute',
    // top: height_logo,
    left: 90,
    color: '#D7BF5E',
    fontSize: 18,
    textDecorationLine: 'underline'
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold'
  },
  text: {
    color: 'grey',
    marginTop: 5
  },
  selectionText: {
    color: '#D7BF5E',
    marginTop: 10,
    top: 10,
    fontSize: tertiary_font_Size,
    textAlign: 'center',
    alignItems: 'center',
  },
  iconFlex: {
    // position: 'absolute',
    // top: 0,
    color: '#D7BF5E',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    // marginTop: 10,
  },
  flex: {
    flex: 1,
    // left: width_user,
    top: selection,
    textAlign: 'center',
    alignItems: 'center',
  },
  mainFlex: {
    flex: 2,
    flexDirection: 'row',
  },
  myProfileHelpFlex: {
    flex: 1,
    // flexDirection: 'row',
    // marginLeft: 10,
  },
  button: {
    alignItems: 'center',
    marginTop: 30,
    bottom: header_margin,
  },
  signIn: {
    width: logOut_button_width,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#7b4d12',
    borderWidth: 1,
    borderStyle: 'solid',
  },
  textSign: {
    color: '#7B4D12FE',
    fontSize: 28,
    textAlign: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontSize: secondary_font_Size,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    color: '#D7BF5E',
    textAlign: 'center',
    letterSpacing: 1.5,

  },
  image: {
    flex: 1,
  },
  profilebtn_txt: {
    color: "#D7BF5E",
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: tertiary_font_Size,
    fontWeight: "800",
    left: 10,
  },
  helpbtn_txt: {
    color: "#D7BF5E",
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: tertiary_font_Size,
    fontWeight: "800",
  },
  profile_margin_top: {
    marginTop: height_logo,
  },
  headerLogoTxt: {
    color: "#D7BF5E",
    width: secondary_font_Size,
    height: secondary_font_Size,
    alignSelf: 'center',
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  contactUsNow: {
    color: '#653507',
    textAlign: 'center',
    marginTop: 2,
    fontSize: middle_font_Size,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  helpBox: {
    height: back_selection,
    width: button_width,
    borderWidth: 2,
    borderColor: 'brown',
    backgroundColor: '#CEFA05',
    borderStyle: 'solid',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    color: '#653507',
    fontSize: tertiary_font_Size,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    // alignContent:'center',
    // textAlign:'center',

  },
  DialogButton: {
    width: image_Width,
    justifyContent: 'space-between',
  },
  DialogYesNo: {
    borderWidth: 2,
    borderColor: 'brown',
    color: 'black',
    marginLeft: fifth_font_Size,
    fontSize: tertiary_font_Size,
    fontFamily: 'Lora-Italic-VariableFont_wght',

  },

});



