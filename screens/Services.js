import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';


const ServicesScreen = ({ navigation }) => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [visible, setVisible] = useState(false);
  // const [showTheThing, setShowTheThing] = useState(false); // normally is put false , temporary put true


  const [data, setShowTheThing] = React.useState({
    showKids: false,
    showFemale: false,
    showMale: false,
    showColour: false,
    showPerm: false,
    showRebonding: false,
    showTreatment: false,

  });
  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000)
  }, []);//the array here meaning when something changes only useeffect will take effect

  if (isLoading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <OrientationLoadingOverlay visible={true}>
          <View>
            {/* <Image source={require('./img/Salon.gif')} style={{ width: 150, height: 150 }} /> */}
            <Image source={require('../img/loading-cat.gif')} style={{ width: 200, height: 200 }} />

          </View>
        </OrientationLoadingOverlay>
      </View>
    )
  }

  const openKids = () => {
    setShowTheThing({
      ...data,
      showKids: !data.showKids,
    })
  }
  const openFemale = () => {
    setShowTheThing({
      ...data,
      showFemale: !data.showFemale,
    })
  }
  const openMale = () => {
    setShowTheThing({
      ...data,
      showMale: !data.showMale,
    })
  }
  const openColour = () => {
    setShowTheThing({
      ...data,
      showColour: !data.showColour,
    })
  }
  const openPerm = () => {
    setShowTheThing({
      ...data,
      showPerm: !data.showPerm,
    })
  }
  const openRebonding = () => {
    setShowTheThing({
      ...data,
      showRebonding: !data.showRebonding,
    })
  }
  const openTreatment = () => {
    setShowTheThing({
      ...data,
      showTreatment: !data.showTreatment,
    })
  }

  return (
    <ImageBackground source={require('../img/Background.png')} resizeMode="cover" style={styles.image}>

      <View>
        <Animatable.Image
          animation="bounceIn"
          source={require('../img/HairDo.png')}
          style={styles.logo}
          resizeMode="stretch"
        />
        <Text style={styles.headerLogoTxt}>Hair Do Salon</Text>
      </View>
      <View style={[styles.aboutUsWrapper, { marginTop: height_logo, width: service_width, backgroundColor: '#0096FF' }]}>
        <Text style={[styles.aboutUsHeader, { color: 'white' }]}>Services & Price List (RM)</Text>
      </View>
      <View style={styles.ourBranchesHeight}>
        <ScrollView>
          <TouchableOpacity style={styles.aboutUsWrapper} onPress={openKids}>
            <Text style={styles.aboutUsHeader}>Kids</Text>
          </TouchableOpacity>

          {data.showKids ?
            <View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Hair Cut</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0, flex: 1 }]}>12</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Wash & Cut</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>25</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Fringe Trim</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>3</Text>
              </View>
            </View>
            :
            <View>

            </View>
          }


          <TouchableOpacity style={styles.aboutUsWrapper} onPress={openFemale}>
            <Text style={styles.aboutUsHeader}>Female</Text>
          </TouchableOpacity>
          {data.showFemale ?
            <View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Hair Cut</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>32</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Wash & Cut</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>50</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Fringe Trim</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>5</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Hair Setting</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>35</Text>
              </View>
            </View>

            :
            <View>

            </View>

          }
          <TouchableOpacity style={styles.aboutUsWrapper} onPress={openMale} >
            <Text style={styles.aboutUsHeader}>Male</Text>
          </TouchableOpacity>
          {data.showMale ?
            <View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Hair Cut</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>32</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Wash & Cut</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>39</Text>
              </View>
            </View>
            :
            <View>

            </View>
          }
          <TouchableOpacity style={styles.aboutUsWrapper} onPress={openColour} >
            <Text style={styles.aboutUsHeader}>Colour</Text>
          </TouchableOpacity>
          {data.showColour ?
            <View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Short</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>100</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Medium</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>160</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Long</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>200</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Touch Up</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>95</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Bleach</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>100</Text>
              </View>
            </View>
            :
            <View>

            </View>
          }


          <TouchableOpacity style={styles.aboutUsWrapper} onPress={openPerm}>
            <Text style={styles.aboutUsHeader}>Perm</Text>
          </TouchableOpacity>
          {data.showPerm ?
            <View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Short</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>100</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Medium</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>150</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Long</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>200</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Extra Long</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>250</Text>
              </View>
            </View>
            :
            <View>

            </View>
          }
          <TouchableOpacity style={styles.aboutUsWrapper} onPress={openRebonding}>
            <Text style={styles.aboutUsHeader}>Rebonding</Text>
          </TouchableOpacity>
          {data.showRebonding ?
            <View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Short</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>160</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Medium</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>200</Text>
              </View>

              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Long</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>250</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Extra Long</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>300</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Touch Up</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>160</Text>
              </View>
            </View>
            :
            <View>

            </View>
          }

          <TouchableOpacity style={[styles.aboutUsWrapper]} onPress={openTreatment}>
            <Text style={styles.aboutUsHeader}>Treatment</Text>
          </TouchableOpacity>
          {data.showTreatment ?
            <View style={{ marginBottom: 20 }}>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Hair & Scalp</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>180</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>SMC Treatment</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>160</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>SMC Special</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>180</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Salon Solution</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0}]}>250</Text>
              </View>
              <View style={styles.mainFlex}>
                <Text style={[styles.ourBranchText, { flex: 3 }]}>Keratin Treatment</Text>
                <View style={styles.verticalLine}></View>
                <Text style={[styles.ourBranchText, { textAlign: 'center', paddingLeft: 0 }]}>160</Text>
              </View>
            </View>
            :
            <View>

            </View>
          }

        </ScrollView>
      </View>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        {/* <View style={[styles.helpBox,styles.backFont, { marginTop: 60 }]}> */}
        <View style={[styles.helpBox, styles.helpBox4]}>
          <Text style={styles.helpBoxText}>Back</Text>
        </View>
      </TouchableOpacity>

    </ImageBackground>
  );
};


export default ServicesScreen;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");
// const height_logo = height * 0.28;
const service_width = width * 1;
const width_logo = width * 0.7;
const width_desc = width * 0.45;
const width_price = width * 0.25;
const width_line = width * 0.2;
const tertiary_font_Size = width * 0.035;

const ourBranchesHeight = height * 0.5;
const height_logo = height * 0.20;
const back_selection = height * 0.04;
const font_size = height * 0.02;



const styles = StyleSheet.create({
  logo: {
    width: 110,
    height: 110,
    alignSelf: 'center',
    position: 'absolute',
    justifyContent: 'center', //horizontal align
    alignSelf: 'center',
    marginTop: 11,

  },
  mainFlex: {
    flex: 4,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'brown',
    borderWidth: 2,
    width: width_logo,
    alignSelf: 'center',
    alignContent: 'center',
  },
  image: {
    flex: 1,
    // justifyContent: "center"
  },
  headerLogoTxt: {
    color: "#D7BF5E",
    fontSize: 15,
    alignSelf: 'center',
    position: 'absolute',
    top: 105,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  aboutUsWrapper: {
    backgroundColor: 'rgba(250, 205, 145, 1)',
    width: width_logo,
    height: back_selection,
    marginTop: 20,
    alignContent: 'center',
    alignSelf: 'center',
  },
  aboutUsHeader: {
    color: '#653507',
    textDecorationLine: 'underline',
    textAlign: 'center',
    marginTop: 5,
    // textTransform: 'uppercase',
    fontSize: tertiary_font_Size,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  HairDo: {
    fontWeight: 'bold',
  },
  ourBranchText: {
    color: 'blue',
    marginTop: 10,
    marginBottom: 10,
    fontSize: font_size,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    backgroundColor: 'white',
    alignSelf: 'center',
    paddingLeft: 5,
    paddingTop: 2,
    flex: 1,
    marginLeft: 20,
    right: 10,
  },
  ourBranchesHeight: {
    height: ourBranchesHeight,
    marginLeft: 20,
    marginRight: 20,
  },
  helpBox4: {
    backgroundColor: '#fd6c9e',
  },
  helpBoxText: {
    textAlign: 'center',
    // marginTop: 10,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: 28,
    color: 'black',
  },
  verticalLine: {
    height: '100%',
    width: 5,
    backgroundColor: 'brown',
    // marginLeft: width_line,
  }
});



