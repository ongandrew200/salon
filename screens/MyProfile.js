import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ImageBackground, ScrollView, TextInput, Picker } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import Dialog from "react-native-dialog";
import { Snackbar } from 'react-native-paper';
import { RadioButton } from 'react-native-paper';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'

const MyProfileScreen = ({ navigation }) => {
  const [visible, setVisible] = useState(false); //register cancel dialog box
  const [backVisible, setBackVisible] = useState(false); //register cancel dialog box
  const [loginSuccessvisible, setLoginSuccessVisible] = useState(false); //login success snackbar
  const [selectedValue, setSelectedValue] = useState("java");
  // const [email, setUsername] = useState('');
  // const [password, setPassword] = useState('');
  const [age, setAge] = useState(1);
  const [phoneNum, setPhoneNum] = useState(0);
  const [checked, setChecked] = React.useState('Female');

  const [data, setData] = React.useState({
    username: '',
    password: '',
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true,
  });

  const onCheckedAge = () => {
    const parsedAge = Number.parseInt(age)
    if (parsedAge < 0) {
      setAge(0);
    }
    else if (parsedAge > 100) {
      setAge(100);

    }
    else {
      setAge(parsedAge);
    }
  }
  const textInputChange = (val) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        username: val,
        check_textInputChange: true,
        isValidUser: true
      });
    } else {
      setData({
        ...data,
        username: val,
        check_textInputChange: false,
        isValidUser: false
      });
    }
  }

  const handlePasswordChange = (val) => {
    if (val.trim().length >= 8) {
      setData({
        ...data,
        password: val,
        isValidPassword: true
      });
    } else {
      setData({
        ...data,
        password: val,
        isValidPassword: false
      });
    }
  }

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry
    });
  }

  const handleValidUser = (val) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        isValidUser: true
      });
    } else {
      setData({
        ...data,
        isValidUser: false
      });
    }
  }

  const showDialog = () => {
    setVisible(true);
  };
  const showBackDialog = () => {
    setBackVisible(true);
  };
  const handleCloseBack = () => {
    setBackVisible(false);
  };
  const showLoginSuccessSnackbar = () => {
    setLoginSuccessVisible(!visible);
  };
  const onDismissSnackBar = () => {
    setLoginSuccessVisible(false)
  };
  const handleCancel = () => {
    setVisible(false);
  };


  return (
    // <ScrollView style={styles.container}>
    <ImageBackground source={require('../img/Background.png')} resizeMode="cover" style={styles.image}>
      <View style={styles.myProfileMainFlex}>
        <View style={styles.Headerflex}>
          <View style={styles.header}>

            <Animatable.Image
              animation="bounceIn"
              source={require('../img/HairDo.png')}
              style={styles.logo}
              resizeMode="stretch"
            />
          </View>
          <Text style={styles.helpbtn_txt}>HairDo Salon</Text>
        </View>
        <View style={styles.Headerflex}>
          <TouchableOpacity onPress={() => navigation.navigate('FAQ')}>
            <Animatable.View
              animation="bounceIn"

            >
              <Feather
                name="help-circle"
                color="white"
                size={44}
              />
              <Text style={styles.helpbtn_txt}>HELP</Text>
            </Animatable.View>
          </TouchableOpacity>
        </View>
      </View>
      <Animatable.View style={styles.timeline} animation="fadeInDown" duration={3000} delay={1000}>
        <Text style={styles.timelineText}>My Profile</Text>
      </Animatable.View>
      <View style={[styles.ourBranchesHeight, { alignSelf: 'center' }]}>
        <View style={[{ height: 128, width: 128 }]}>
          <Image style={styles.ourBranchesImage} source={require('../img/KarenGillan.jpg')} />
        </View>
        <ScrollView style={[styles.myProfileContent, { alignSelf: 'center', marginTop:10}]}>
          <View style={[styles.mainFlex, { paddingLeft: 10, paddingRight: 10 }]}>


            <View style={styles.ourBranchText}>
              <View style={styles.container}>

                <Animatable.View
                  animation="fadeInUpBig"
                  style={[styles.footer, {
                    // backgroundColor: colors.background
                  }]}
                >
                  <Text style={[styles.text_footer, {
                    color: '#683309',
                    // color: colors.text
                  }]}>Name: </Text>
                  {/* <View style={styles.action}> */}
                  <View>
                    <TextInput
                      placeholder="Enter your username"
                      placeholderTextColor="#347b5d"
                      style={[styles.textInput, {
                        color: '#A1045A'
                        // color: colors.text
                      }]}
                      autoCapitalize="none"
                      onChangeText={(val) => textInputChange(val)}
                      onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                      type="email"
                    />
                    {data.check_textInputChange ?
                      <Animatable.View
                        animation="bounceIn"
                      >
                        <Feather
                          name="check-circle"
                          color="green"
                          size={20}
                        />
                      </Animatable.View>
                      : null}
                  </View>
                  {data.isValidUser ? null :
                    <Animatable.View animation="fadeInLeft" duration={500}>
                      <Text style={[styles.errorMsg, { color: 'white' }]}>*Username must be 4 characters long.</Text>
                    </Animatable.View>
                  }


                  <Text style={[styles.text_footer, {
                    color: '#683309',
                    // color: colors.text,
                    marginTop: 20
                  }]}>Age :</Text>
                  <View style={styles.action}>
                    <TextInput
                      placeholder="Your Age"
                      placeholderTextColor="#347b5d"
                      style={[styles.textInput, {
                        color: '#A1045A'
                        // color: colors.text
                      }]}
                      keyboardType='numeric'
                      onChangeText={(val) => {
                        if (isNaN(val)) {
                          console.log(val);
                          setAge(0)
                          // console.log(age);
                          // console.log(target.valueAsNumber);
                        }
                        else if (val == ' ') {
                          setAge(NaN)
                        }
                        else {
                          let numberValue = parseInt(val)
                          setAge(Math.max(1, Math.floor(numberValue)))

                        }

                      }}
                      InputProps={{ inputProps: { min: 1 } }}
                      type="number"
                      // onChangeText={(age)=>onCheckedAge(age)}
                      maxLength={2}
                      value={isNaN(age) ? '' : age.toString()}

                    />
                  </View>
                  <Text style={[styles.text_footer, {
                    color: '#683309',
                    // color: colors.text,
                    marginTop: 20
                  }]}>Gender :</Text>
                  <View style={[styles.mainFlex, , { marginTop: 0 }]}>

                    <RadioButton
                      value="Female"
                      label="Female"
                      status={checked === 'Female' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('Female')}
                      style={styles.flex}
                    />
                    <Text style={[styles.RadioButtonText, { color: '#683309' }]}>Female</Text>
                    <RadioButton
                      value="Male"
                      label="Male"
                      status={checked === 'Male' ? 'checked' : 'unchecked'}
                      onPress={() => setChecked('Male')}
                    />
                    <Text style={[styles.RadioButtonText, { color: '#683309' }]}>Male</Text>

                  </View>

                  <Text style={[styles.text_footer, {
                    color: '#683309',
                    // color: colors.text,
                    marginTop: 20
                  }]}>Phone Number :</Text>
                  <View style={styles.action}>
                    <TextInput
                      placeholder="Phone Number (No - ) "
                      placeholderTextColor="#347b5d"
                      style={[styles.textInput, {
                        color: '#A1045A',
                        marginBottom: 20,
                        // color: colors.text
                      }]}
                      keyboardType='numeric'
                      autoCapitalize="none"
                      // onChangeText={({ target }) => setAge(Math.max(1, Math.floor((target.valueAsNumber) ? Number(target.valueAsNumber) : target.valueAsNumber)))}
                      onChangeText={(val) => setPhoneNum(val)}
                      maxLength={10}
                    />
                  </View>
                  <Animatable.View style={[styles.button, { marginTop: 0, marginBottom: 15 }]} animation="fadeInUpBig">
                    <TouchableOpacity
                      style={styles.signIn}
                      // onPress={() => { loginHandle(data.username, data.password) }} //to process login 
                      onPress={() => {
                        navigation.navigate('SuccessScreen');
                        setLoginSuccessVisible(true)
                      }} //tempoprary
                    >
                      <LinearGradient
                        colors={['#ffff80', '#ffff80']}
                        style={styles.signIn}
                      >
                        <SimpleLineIcons
                          name="note"
                          size={20}
                        />
                        <Text style={styles.textSign}>Update Profile</Text>
                      </LinearGradient>
                    </TouchableOpacity>

                    {/* <TouchableOpacity onPress={showDialog}>
                        <LinearGradient
                          colors={['#facd91', '#facd91']}
                          style={[styles.signIn, { marginTop: 15 }]}
                        >
                          <Text style={styles.textSign}>Cancel
                          </Text>
                        </LinearGradient>
                      </TouchableOpacity> */}

                  </Animatable.View>
                </Animatable.View>
                <Dialog.Container visible={backVisible}>
                  <Dialog.Title>
                    Are you sure you want to cancel?
                  </Dialog.Title>
                  <Dialog.Title>
                    The data key will be clear?
                  </Dialog.Title>
                  <Dialog.Button label="Yes" onPress={() => {
                    navigation.goBack();
                    // setLogOutSuccess(true);
                  }} />
                  <Dialog.Button label="No" onPress={handleCloseBack} />
                </Dialog.Container>
                <Snackbar
                  visible={loginSuccessvisible}
                  onDismiss={onDismissSnackBar}
                >
                  Hey there! I'm a Snackbar.
                </Snackbar>
              </View>
            </View>
          </View>

        </ScrollView>
      </View>
      <View style={styles.mainFlex}>
        <Animatable.View style={styles.flex} animation="zoomInDown" duration={2000} delay={1000}>
          <TouchableOpacity onPress={() => navigation.navigate('RequestBookingScreen')}>

            <AntDesign
              name="calendar"
              size={50}
              style={[styles.iconFlex, { left: 20 }]}
            />
            <Text style={styles.selectionText}>Request Booking</Text>
          </TouchableOpacity>
        </Animatable.View>
        <Animatable.View style={styles.flex} animation="zoomInRight" duration={2000} delay={1000}>
          <TouchableOpacity onPress={() => navigation.navigate('ServicesScreen')}>

            <FontAwesome
              name="scissors"
              size={50}
              style={[styles.iconFlex]}
            />
            <Text style={styles.selectionText}>Services</Text>
          </TouchableOpacity>
        </Animatable.View>
      </View>
      <View style={styles.mainFlex}>
        <Animatable.View style={styles.flex} animation="zoomInDown" duration={2000} delay={1000}>

          <TouchableOpacity
            onPress={showBackDialog}
            style={[styles.button, styles.Buttonflex]}
          >
            {/* <View style={[styles.helpBox,styles.backFont, { marginTop: 60 }]}> */}
            <View style={[styles.helpBox, styles.helpBox4]}>
              <Text style={styles.helpBoxText}>Back</Text>
            </View>
          </TouchableOpacity>
        </Animatable.View>

        <Animatable.View style={styles.flex} animation="zoomInDown" duration={2000} delay={1000}>

          <TouchableOpacity
            onPress={showDialog}
            style={[styles.button, styles.Buttonflex]}
          >
            {/* <View style={[styles.helpBox,styles.backFont, { marginTop: 60 }]}> */}
            <View style={[styles.helpBox, styles.helpBox4]}>
              <Text style={styles.helpBoxText}>LogOut</Text>
            </View>
          </TouchableOpacity>
        </Animatable.View>
        <Dialog.Container visible={visible}>

          <Dialog.Title>
            Are you sure you wanna log out?
          </Dialog.Title>
          <Dialog.Button label="Yes" onPress={() => {
            navigation.navigate('SplashScreen');
            // setLogOutSuccess(true);
          }} />
          <Dialog.Button label="No" onPress={handleCancel} />
        </Dialog.Container>
      </View>

    </ImageBackground>


  );
};


export default MyProfileScreen;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");
// const height_logo = height * 0.28;
const button_width = width * 0.40;
const logOut_button_width = width * 0.75;
const height_logo = height * 0.15;
const selection = height * 0.05;
const footerButton = height * 0.01;
const width_logo = width * 0.4;
const width_user = width * 0.1;
const width_user_text = width * 0.3;
const ourBranchesHeight = height * 0.48;
const help_width = width * 0.3;
const back_selection = height * 0.03;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    alignItems: 'center'
  },
  logo: {
    width: 100,
    height: 75,
    // alignItems: 'flex-start',
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold'
  },
  timeline: {
    backgroundColor: '#d9d9d9',
    height: 46,
  },

  timelineText: {
    textAlign: 'center',
    fontSize: 30,
    color: '#7B4D12FE',
    fontWeight: "600",
    textDecorationLine: 'underline',
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  text: {
    color: 'grey',
    marginTop: 5
  },
  selectionText: {
    color: '#D7BF5E',
    top: 10,
    fontSize: 18,
    textAlign: 'center',
    alignItems: 'center',
  },
  iconFlex: {
    color: '#D7BF5E',
  },
  flex: {
    flex: 1,
    // left: width_user,
    // top: selection,
    textAlign: 'center',
    alignItems: 'center',
  },
  Headerflex: {
    flex: 1,
    // left: width_user,
    top: footerButton,
    textAlign: 'center',
    alignItems: 'center',
  },

  Buttonflex: {
    flex: 1,
    // left: width_user,
    // top: footerButton,
    textAlign: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  mainFlex: {
    flex: 2,
    flexDirection: 'row',
    marginTop: 20,
  },
  myProfileMainFlex: {
    flex: 3,
    flexDirection: 'row',
  },
  button: {
    alignItems: 'center',
    marginTop: 30,
    // bottom: selection,
  },
  myProfileContent: {
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 10,
    backgroundColor: 'rgba(248, 192, 108, 0.8)',
  },
  signIn: {
    width: logOut_button_width,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: '#7b4d12',
    borderWidth: 1,
    borderStyle: 'solid',
  },
  textSign: {
    color: '#7B4D12FE',
    fontSize: 28,
    textAlign: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontSize: 40,
    fontFamily: 'Cochin',
    color: '#7B4D12FE',
    fontWeight: 'bold',
    textDecorationLine: 'underline'
  },
  image: {
    flex: 1,
    // justifyContent: "center"
  },
  helpbtn_txt: {
    color: 'white',
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: 15,
    fontWeight: "800",
  },
  ourBranchesHeight: {
    height: ourBranchesHeight
  },
  ourBranchesImage: {
    width: 128,
    height: 128,
    borderRadius: 128 / 2,
    marginTop: 10,
    flex: 1,
    marginRight: 20,
  },
  ourBranchText: {
    color: 'black',
    // marginTop: 20,
    fontSize: 15,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    // width: width_user_text,
    alignSelf: 'center',
  },
  helpBox: {
    height: 45,
    width: button_width,
    borderColor: 'black',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    bottom: back_selection,
  },
  helpBox4: {
    backgroundColor: '#fd6c9e',
  },
  helpBoxText: {
    textAlign: 'center',
    // marginTop: 10,
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: 28,
    color: 'black',
  },
  textInput: {
    backgroundColor: 'white',
    borderColor: 'brown',
    borderRadius: 5,
    borderWidth: 2,
    height: 40,
    paddingLeft: 10,
    width: logOut_button_width,
  },
  RadioButtonText: {
    color: 'white',
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: 15,
    top: 7,
  }
});



