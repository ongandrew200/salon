import React, { useState, useEffect } from 'react';
import { Button, View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import * as Animatable from 'react-native-animatable';

const TimeDisplay = () => {
  // const [currentDate, setCurrentDate] = useState('');
  const [currentDate, setCurrentDate] = useState('');
  const [hrFmt, setHrFmt] = useState('');
  useEffect(() => {
    let secTimer = setInterval(() => {

      var date = new Date().getDate(); //Current Date
      var month = new Date().getMonth() + 1; //Current Month
      var year = new Date().getFullYear(); //Current Year
      var hours = new Date().getHours(); //Current Hours
      var min = new Date().getMinutes(); //Current Minutes
      var sec = new Date().getSeconds(); //Current Minutes

      if (min < 10) {
        min = '0' + min;
      }
      if (sec < 10) {
        sec = '0' + sec;
      }

      if (hours > 12) {
        setHrFmt('PM');
        hours = hours - 12;

      }
      else if (hours < 12) {
        setHrFmt('AM');
      }

      var day = new Date().getDay();
      switch (new Date().getDay()) {
        case 0:
          day = "Sunday";
          break;
        case 1:
          day = "Monday";
          break;
        case 2:
          day = "Tuesday";
          break;
        case 3:
          day = "Wednesday";
          break;
        case 4:
          day = "Thursday";
          break;
        case 5:
          day = "Friday";
          break;
        case 6:
          day = "Saturday";
      }

      setCurrentDate(
        hours + ':' + min + ':' + sec + ' ' + hrFmt + ' , ' + day + ' , ' + date + '/' + month + '/' + year
      )

    }, 500)

    return () => clearInterval(secTimer);
    //JS


  }, []);
  return (
    <Animatable.View style={styles.timeline} animation="bounceInRight" duration={3000} delay={500}>
      <Text style={styles.timelineText}>{currentDate}</Text>
    </Animatable.View>


  );
};
export default TimeDisplay;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");

const selection = height * 0.03;
const tertiary_font_Size = width * 0.035;

const styles = StyleSheet.create({

  timeline: {
    backgroundColor: '#D7BF5E',
    height: selection,
    marginTop: 20,
    justifyContent: 'center',//vertically align content to center
  },

  timelineText: {
    textAlign: 'center',
    fontSize: tertiary_font_Size,
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
});