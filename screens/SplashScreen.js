import React, { useState, useEffect } from 'react';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { View, Text, StyleSheet, Dimensions, Image, TouchableOpacity, ImageBackground, ScrollView, StatusBar } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import Feather from 'react-native-vector-icons/Feather';
import { brown } from 'color-name';
const SplashScreen = ({ navigation }) => {

    return (
        <ImageBackground source={require('../img/Luxury_Background.jpg')} resizeMode="stretch" style={styles.image}>
            <StatusBar hidden />
            {/* Hide Status bar */}
            {/* <View style={styles.container}> */}
            <ScrollView>
                <View>
                    <View style={[styles.header,{marginTop:10}]}>
                        <Animatable.Image
                            animation="bounceIn"
                            source={require('../img/HairDo.png')}
                            style={styles.logo}
                            resizeMode="stretch"
                            duration={2000}
                            delay={500}
                        />
                        <Text style={styles.headerText}>HairDo Saloon App</Text>
                        <Text style={styles.secondaryText}>Come Experience the Difference</Text>

                    </View>
                    <View style={styles.header}>
                        <Animatable.View style={styles.button} animation="fadeInUpBig">
                            <TouchableOpacity onPress={() => navigation.navigate('SignInScreen')}>
                                <LinearGradient
                                    colors={['#36d1dc', '#5b86e5']}
                                    style={styles.signIn}
                                >
                                    <Text style={styles.textSign}>LOGIN
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('SignUpScreen')}>
                                <LinearGradient
                                    colors={['#ec6f66', '#f3a183']}
                                    style={[styles.signIn, styles.marginRegister]}

                                >
                                    <Text style={styles.textSign}>REGISTER
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigation.navigate('FAQ')}>
                                <LinearGradient
                                    colors={['#36d1dc', '#5b86e5']}
                                    style={[styles.signIn, styles.marginRegister]}
                                >
                                    <Text style={styles.textSign}>HELP
                                    </Text>
                                </LinearGradient>
                            </TouchableOpacity>
                        </Animatable.View>
                    </View>

                    {/* </Animatable.View> */}
                </View>
            </ScrollView>
        </ImageBackground>

    );
};


export default SplashScreen;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");
const logo_height_post = height * 0.05;
const image_Width = width * 0.6;
const button_top = height * 0.15;
const image_height = height * 0.3;
const font_Size = width * 0.07;
const secondary_font_Size = width * 0.05;
const tertiary_font_Size = width * 0.04;

const styles = StyleSheet.create({
    helpbtn: {
        alignItems: 'flex-end',
        marginRight: 10,
        marginTop: 20,
    },
    helpbtn_txt: {
        color: "white",
        fontFamily: 'Lora-Italic-VariableFont_wght',
        fontSize: 15,
        fontWeight: "800",
    },
    header: {
        alignItems: 'center',
        marginTop:button_top,
    },
    footer: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingVertical: 50,
        paddingHorizontal: 30
    },
    logo: {
        width: image_Width,
        height: image_height,
        },
    title: {
        color: '#05375a',
        fontSize: 30,
        fontWeight: 'bold'
    },
    text: {
        color: 'grey',
        marginTop: 5
    },
    button: {
        alignItems: 'center',
        marginTop: 50,
    },
    signIn: {
        width: image_Width,
        height: logo_height_post,
        justifyContent: 'center', //horizontal align
        alignItems: 'center',//vertical align
        borderRadius: 15,
        flexDirection: 'row'
    },
    textSign: {
        // color: '#7B4D12FE',
        color: 'white',
        fontSize: tertiary_font_Size,
        fontFamily: 'Lora-Italic-VariableFont_wght',
        textAlign: 'center',
        justifyContent: 'center',
    },
    headerText: {
        fontSize: font_Size,
        color: '#ffd700',
        fontFamily: 'Lora-Italic-VariableFont_wght',
        fontWeight: "600",
        textDecorationLine: 'underline',
        letterSpacing: 1,
    },
    image: {
        flex: 1,
    },
    marginRegister: {
        marginTop: 30,
    },
    secondaryText: {
        fontSize: secondary_font_Size,
        fontFamily: 'Billabong',
        color: '#ffd700',
        letterSpacing: 2,
        marginTop: 5,
    },

});



