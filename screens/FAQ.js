import React, { useState, useEffect, Children } from 'react';
import { View, StyleSheet, Dimensions, Text, Image, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import * as Animatable from 'react-native-animatable';
import { DefaultTheme, Dialog, Paragraph, Portal } from 'react-native-paper';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { AuthContext } from '../component/AuthProvider';


const FAQ = ({ navigation }) => {
  const [registerLoginVisible, setRegisterLoginVisible] = useState(false); //register cancel dialog box
  const [forgetPasswordVisible, setForgetPasswordVisible] = useState(false); //register cancel dialog box
  const [requestBookingsVisible, setRequestBookingsVisible] = useState(false); //register cancel dialog box
  const [editBookingsVisible, setEditBookingsVisible] = useState(false); //register cancel dialog box
  const [myProfileVisible, setMyProfileVisible] = useState(false); //register cancel dialog box


  const showRegisterLoginDialog = () => {
    setRegisterLoginVisible(true);
  };
  const hideRegisterLoginDialog = () => {
    setRegisterLoginVisible(false);
  };
  const showForgetPasswordDialog = () => {
    setForgetPasswordVisible(true);
  };
  const hideForgetPasswordDialog = () => {
    setForgetPasswordVisible(false);
  };
  const showRequestBookingsVisible = () => {
    setRequestBookingsVisible(true);
  };
  const hideRequestBookingsVisible = () => {
    setRequestBookingsVisible(false);
  };
  const showEditBookingsVisible = () => {
    setEditBookingsVisible(true);
  };
  const hideEditBookingsVisible = () => {
    setEditBookingsVisible(false);
  };
  const showMyProfileVisible = () => {
    setMyProfileVisible(true);
  };
  const hideMyProfileVisible = () => {
    setMyProfileVisible(false);
  };
  return (
    <ImageBackground source={require('../img/HomeScreenBackground.jpg')} resizeMode="cover" style={{ flex: 1 }}>

      <ScrollView>
        <View style={{ alignItems: 'center' }}>
          <Animatable.Image
            animation="bounceIn"
            source={require('../img/HairDo.png')}
            style={styles.logo}
            resizeMode="cover"
          />
        </View>
        <Animatable.View style={styles.timeline} animation="bounceInRight" duration={3000} delay={1000}>
          <Text style={styles.timelineText}>HairDo FAQ</Text>
        </Animatable.View>
        <Animatable.View style={styles.mainFlex} animation="fadeInUpBig" duration={1000} delay={1000}>
          <TouchableOpacity onPress={showRegisterLoginDialog}>
            <LinearGradient colors={['white', '#f4dffe']}>

              <View style={styles.helpBox}>
                <Text style={[styles.helpBoxText, styles.helpBoxText1]}>REGISTER & LOGIN</Text>
              </View>
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity onPress={showForgetPasswordDialog}>
            <LinearGradient colors={['#f4dffe', '#e9bffd']}>

              <View style={styles.helpBox}>
                <Text style={[styles.helpBoxText, styles.helpBoxText1]}>FORGET PASSWORD</Text>
              </View>
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity onPress={showRequestBookingsVisible} >
            <LinearGradient colors={['#e9bffd', '#df9efd']}>

              <View style={styles.helpBox}>
                <Text style={[styles.helpBoxText, styles.helpBoxText1]}>REQUEST BOOKINGS</Text>
              </View>
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity onPress={showEditBookingsVisible}>
            <LinearGradient colors={['#df9efd', '#d47efc']}>

              <View style={styles.helpBox}>
                <Text style={[styles.helpBoxText, styles.helpBoxText1]}>EDIT BOOKINGS</Text>
              </View>
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity onPress={showMyProfileVisible}>
            <LinearGradient colors={['#d47efc', '#cd6efb']}>

              <View style={styles.helpBox}>
                <Text style={[styles.helpBoxText, styles.helpBoxText1]}>MY PROFILE COMPLETION</Text>
              </View>
            </LinearGradient>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate('AboutUsScreen')}>

            <View style={[styles.helpBox, styles.helpBox3, { marginTop: 20 }]}>
              <Text style={[styles.helpBoxText, styles.helpBoxText2]}>ABOUT US</Text>

            </View>

          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.goBack()}>
            {/* <View style={[styles.helpBox,styles.backFont, { marginTop: 60 }]}> */}
            <View style={[styles.helpBox, styles.helpBox4, { marginTop: 60 }]}>
              <Text style={[styles.helpBoxText, styles.helpBoxText2]}>Back</Text>
            </View>
          </TouchableOpacity>
        </Animatable.View>

        <Dialog visible={registerLoginVisible} onDismiss={hideRegisterLoginDialog} style={[styles.RegisterLogin, styles.DialogHeight]} >
          <Dialog.ScrollArea>
            <Dialog.Title style={styles.RegisterLoginBar}>
              REGISTER & LOGIN
            </Dialog.Title>
            <Paragraph style={styles.RegisterLoginBarContent}>To close,You just have to click outside the box</Paragraph>
            <ScrollView>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>Before Login into the system , make sure you have registered yourself.</Paragraph>
                <Paragraph style={styles.RegisterLoginBarContentHeader}>(A) You can Register at Register Page</Paragraph>
              </Dialog.Content>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>(1) Fill in the information in the column listed</Paragraph>
                <Paragraph >
                  <AntDesign
                    name="doubleright"
                    size={10}
                  />
                  Username, Phone number , Password, Confirm password
                </Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(2) Press the Button 'Register' and you may registered yourself successfully</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(3) If you want to cancel the Register process, press the Button 'Cancel'. The data key in will be cleared. </Paragraph>
                <View style={styles.bottomLine}></View>
              </Dialog.Content>
              {/*Register Login Part (B)------------------------------------------------------------------------------------ */}
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph style={styles.RegisterLoginBarContentHeader}>(B) You can Login into system at Login Page</Paragraph>
              </Dialog.Content>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>(1) Fill in the information in the column listed</Paragraph>
                <Paragraph >
                  <AntDesign
                    name="doubleright"
                    size={10}
                  />
                  Username, Password
                </Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(2) Press the Button 'Register' and you may registered yourself successfully</Paragraph>
              </Dialog.Content>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>

        <Dialog visible={forgetPasswordVisible} onDismiss={hideForgetPasswordDialog} style={[styles.RegisterLogin, styles.DialogHeight]} >
          <Dialog.ScrollArea>
            <Dialog.Title style={styles.RegisterLoginBar}>
              FORGOT PASSWORD
            </Dialog.Title>
            <Paragraph style={styles.RegisterLoginBarContent}>To close,You just have to click outside the box</Paragraph>
            <ScrollView>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>If you forgot your password, you may reset it by selecting "Forget Password"</Paragraph>
                <Paragraph style={styles.RegisterLoginBarContentHeader}>(A) Verify the phone number</Paragraph>
              </Dialog.Content>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>(1) Fill in the phone number you have registered previously</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(2)  Press the Button 'Send' to get the verificaton code.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(3)  After verification, it will redirect you to the password reset page.</Paragraph>
                <View style={styles.bottomLine}></View>
              </Dialog.Content>
              {/*Forgot Password Part (B)------------------------------------------------------------------------------------ */}
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph style={styles.RegisterLoginBarContentHeaderPartB}>(B) Resetting your password</Paragraph>
              </Dialog.Content>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph style={styles.RegisterLoginText}>(1) Fill in your new password and matching your confirm password.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(2) Make sure the password you entered is different from the old ones.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(3) You may proceed to login with your new password !</Paragraph>

              </Dialog.Content>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>
        <Dialog visible={requestBookingsVisible} onDismiss={hideRequestBookingsVisible} style={[styles.RegisterLogin, styles.DialogHeight]} >
          <Dialog.ScrollArea>
            <Dialog.Title style={styles.RegisterLoginBar}>
              REQUEST BOOKINGS
            </Dialog.Title>
            <Paragraph style={styles.RegisterLoginBarContent}>To close,You just have to click outside the box</Paragraph>
            <ScrollView>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>You can request for booking in the "Request Bookings" tab</Paragraph>
                <Paragraph style={styles.RegisterLoginBarContentHeader}>(A) How to request for booking</Paragraph>
              </Dialog.Content>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>(1) Select the dates, time and services you wish to have.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(2) Confirm the booking.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(3) You may view your booking now in the "My Bookings" tab.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(4) Your profile details will be used when the bookings is created.</Paragraph>
                <View style={styles.bottomLine}></View>
              </Dialog.Content>
              {/*Request Bookings Part (B)------------------------------------------------------------------------------------ */}
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph style={styles.RegisterLoginBarContentHeaderPartB}>(B) Pressing Cancel</Paragraph>
              </Dialog.Content>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph style={styles.RegisterLoginText}>(1) Please bear in mind that pressing the "Cancel" button will clear the form and reload the page.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(2) You may re-enter the details if you do it wrongly.</Paragraph>

              </Dialog.Content>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>
        <Dialog visible={editBookingsVisible} onDismiss={hideEditBookingsVisible} style={[styles.RegisterLogin, styles.DialogHeight]} >
          <Dialog.ScrollArea>
            <Dialog.Title style={styles.RegisterLoginBar}>
              EDITTING BOOKINGS
            </Dialog.Title>
            <Paragraph style={styles.RegisterLoginBarContent}>To close,You just have to click outside the box</Paragraph>
            <ScrollView>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>You may Edit or Delete the booking that you have requested in "My Bookings" tab.</Paragraph>
                <Paragraph style={styles.RegisterLoginBarContentHeader}>(A) Verify the phone number</Paragraph>
              </Dialog.Content>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>(1) Fill in the phone number you have registered previously</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(2) Press the Button 'Send' to get the verificaton code.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(3) After verification, it will redirect you to the password reset page.</Paragraph>
                <View style={styles.bottomLine}></View>
              </Dialog.Content>
              {/*Editting Bookings Part (B)------------------------------------------------------------------------------------ */}
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph style={styles.RegisterLoginBarContentHeaderPartB}>(B) Resetting your password</Paragraph>
              </Dialog.Content>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph style={styles.RegisterLoginText}>(1) Fill in your new password and matching your confirm password.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(2) Make sure the password you entered is different from the old ones.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(3) You may proceed to login with your new password !</Paragraph>

              </Dialog.Content>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>
        <Dialog visible={myProfileVisible} onDismiss={hideMyProfileVisible} style={[styles.RegisterLogin, styles.DialogHeight]} >
          <Dialog.ScrollArea>
            <Dialog.Title style={styles.RegisterLoginBar}>
              MY PROFILE
            </Dialog.Title>
            <Paragraph style={styles.RegisterLoginBarContent}>To close,You just have to click outside the box</Paragraph>
            <ScrollView>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>User is encouraged to complete your profile in "My Profile" tab after they registered.</Paragraph>
                <Paragraph style={styles.RegisterLoginBarContentHeader}>(A) Verify the phone number</Paragraph>
              </Dialog.Content>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph>(1) Fill in the phone number you have  registered previously</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(2) Press the Button 'Send' to get the verificaton code.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(3) After verification, it will redirect you to the password reset page.</Paragraph>
                <View style={styles.bottomLine}></View>
              </Dialog.Content>
              {/*Editting Bookings Part (B)------------------------------------------------------------------------------------ */}
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph style={styles.RegisterLoginBarContentHeaderPartB}>(B) Resetting your password</Paragraph>
              </Dialog.Content>
              <Dialog.Content style={styles.RegisterLoginBarContent}>
                <Paragraph style={styles.RegisterLoginText}>(1) Fill in your new password and matching your confirm password.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(2) Make sure the password you entered is different from the old ones.</Paragraph>
                <Paragraph style={styles.RegisterLoginText}>(3) You may proceed to login with your new password !</Paragraph>
              </Dialog.Content>
            </ScrollView>
          </Dialog.ScrollArea>
        </Dialog>

      </ScrollView>
    </ImageBackground>

  );
};


export default FAQ;
const { height } = Dimensions.get("screen");
const { width } = Dimensions.get("screen");
// const height_logo = height * 0.28;
const button_width = width * 0.7;
const button_height = height * 0.06;
const DialogTop = height * 0.25;
const height_Dialog = height * 0.75;
const font_Size = width * 0.03;
const help_width = width * 0.2;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    alignItems: 'center'
  },
  logo: {
    width: help_width,
    height: help_width,
  },

  timeline: {
    backgroundColor: '#d9d9d9',
    height: 46,
  },

  timelineText: {
    textAlign: 'center',
    fontSize: 30,
    color: '#7B4D12FE',
    fontWeight: "600",
    textDecorationLine: 'underline',
    fontFamily: 'Lora-Italic-VariableFont_wght',
  },
  image: {
    flex: 1,
  },
  mainFlex: {
    flexDirection: 'column',//same as default value , i know
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  helpBox: {
    height: button_height,
    width: button_width,
    borderColor: 'black',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 5,
    justifyContent: 'center',//vertically align content to center
  },
  helpBox3: {
    backgroundColor: '#e4007c',
  },
  helpBox4: {
    backgroundColor: '#fd6c9e',
  },
  helpBoxText: {
    textAlign: 'center',
    fontFamily: 'Lora-Italic-VariableFont_wght',
    fontSize: font_Size,
    color: 'black',
    // marginVertical:'center',
  },
  helpBoxText1: {
    color: 'black',
  },
  helpBoxText2: {
    color: 'white',
  },
  backFont: {
    fontSize: 18,
    textAlign: 'center',
    marginTop: 10,
    fontFamily: 'Lora-Italic-VariableFont_wght',

  },
  RegisterLogin: {
    backgroundColor: 'rgba(248, 192, 108, 0.8)',
  },
  RegisterLoginBar: {
    backgroundColor: '#d9d9d9',
    textAlign: 'center',
    fontSize: 22,
    color: '#333333',
    fontWeight: "600",
    fontFamily: 'Lora-Italic-VariableFont_wght',
    textTransform: 'uppercase',
  },
  RegisterLoginBarContent: {
    // textAlign: 'center',
    fontSize: 13,
    color: '#000000',
    fontWeight: "400",
    fontFamily: 'Lora-Italic-VariableFont_wght',

  },
  RegisterLoginBarContentHeader: {
    fontWeight: 'bold',
    marginTop: 20,
    textDecorationLine: 'underline',
  },
  RegisterLoginBarContentHeaderPartB: {
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
  RegisterLoginText: {
    marginTop: 25,
  },
  DialogHeight: {
    height: height_Dialog,
    marginTop: DialogTop,
  },
  bottomLine: {
    borderBottomColor: 'black',
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 1,
    marginTop: 10,
  }
});


