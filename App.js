import React, { useEffect } from 'react';
import 'react-native-gesture-handler';
import { View, Text, StyleSheet, Button, Image, ActivityIndicator } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from './screens/HomeScreen';
import DetailScreen from './screens/DetailScreen';
import SplashScreen from './screens/SplashScreen';
import SignInScreen from './screens/SignInScreen';
import SignUpScreen from './screens/SignUpScreen';
import HelpScreen from './screens/HelpScreen';
import ForgotPasswordScreen from './screens/ForgetPassword';

import AuthContext from './component/AuthProvider';

import MyProfileScreen from './screens/MyProfile';
import RequestBookingScreen from './screens/RequestBooking';
import ServicesScreen from './screens/Services';
import MyBookingScreen from './screens/MyBookings';
import AboutUsScreen from './screens/AboutUs';
import FAQ from './screens/FAQ';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';

const HomeStack = createStackNavigator();
const DetailStack = createStackNavigator();

const RootStack = createStackNavigator();

const App = () => {
  const [userToken, setUserToken] = React.useState(null);

  return (
    // <AuthContext.Provider value={authContext}>
    <NavigationContainer>

      <RootStack.Navigator headerMode='none'>
        <RootStack.Screen name="SplashScreen" component={SplashScreen} />
        <RootStack.Screen name="SignInScreen" component={SignInScreen} />
        <RootStack.Screen name="SignUpScreen" component={SignUpScreen} />
        <RootStack.Screen name="SuccessScreen" component={HomeScreen} />
        <RootStack.Screen name="HelpScreen" component={HelpScreen} />
        <RootStack.Screen name="MyProfileScreen" component={MyProfileScreen} />
        <RootStack.Screen name="RequestBookingScreen" component={RequestBookingScreen} />
        <RootStack.Screen name="ServicesScreen" component={ServicesScreen} />
        <RootStack.Screen name="MyBookingScreen" component={MyBookingScreen} />
        <RootStack.Screen name="AboutUsScreen" component={AboutUsScreen} />
        <RootStack.Screen name="FAQ" component={FAQ} />
        <RootStack.Screen name="ForgotPassword" component={ForgotPasswordScreen} />
      </RootStack.Navigator>


    </NavigationContainer>
    // </AuthContext.Provider >
  );
};

export default App;
